﻿using BAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace Pharma_Mart_Admin.Controllers
{
    public class HomeController : Controller
    {
        IProductRepository productRepo = new ProductRepository();

        public ActionResult Index()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<OrdersList> authorizedOrders = productRepo.getAuthorizedOrders();
                ViewBag.AuthorizedCount = authorizedOrders.Count();

                List<OrdersList> pickupOrders = productRepo.getPickupOrders();
                ViewBag.PickupCount = pickupOrders.Count();

                List<OrdersList> dispatchedOrders = productRepo.getDispatchedOrders();
                ViewBag.DispatchedCount = dispatchedOrders.Count();

                List<OrdersList> deliveredOrders = productRepo.getDeliveredOrders();
                ViewBag.DeliveredCount = deliveredOrders.Count();

                List<Tbl_Product_Info> products = productRepo.getAllProducts();
                ViewBag.ProductsCount = products.Count();

                List<Tbl_Product_Info> inActiveProducts = productRepo.getAllInActiveProducts();
                ViewBag.InActiveProductsCount = inActiveProducts.Count();

                ViewBag.TotalProductsCount = products.Count() + inActiveProducts.Count();

                List<Tbl_User_Info> users = productRepo.getAllUsers();
                ViewBag.UsersCount = users.Count();

                List<Tbl_User_Info> monthlyUsersCount = users.Where(x => Convert.ToDateTime(x.Created_Date.Value).Month == DateTime.Now.Month).ToList();
                ViewBag.monthlyUsersCount = monthlyUsersCount.Count();

                List<Tbl_User_Info> weeklyUsersCount = users.Where(x => Convert.ToDateTime(x.Created_Date.Value).Date >= DateTime.Now.Date.AddDays(-7)).ToList();
                ViewBag.weeklyUsersCount = weeklyUsersCount.Count();

                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string UserName, string Password)
        {
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                ViewBag.LoginError = "Please enter Username and Password...";
                return RedirectToAction("Login");
            }
            else
            {
                Tbl_User_Info userData = productRepo.userLogin(UserName, Password);
                if (userData == null)
                {
                    ViewBag.LoginError = "Invalid Username and Password...";
                    return RedirectToAction("Login");
                }
                else
                {
                    Session["login"] = userData;
                    return RedirectToAction("Index");
                }
            }
        }

        public ActionResult Logout()
        {
            if (Session != null)
                Session.Clear();

            return RedirectToAction("Login");
        }

        public JsonResult CategoriesBySuperCategory(int superCatId)
        {
            List<Def_Categories> categories = productRepo.getCategoriesBySuperCategory(superCatId);
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubCategoriesByCategory(int CatId)
        {
            List<Def_Sub_Categories> subCategories = productRepo.getSubCategoriesByCategory(CatId);
            return Json(subCategories, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProductSubType(int TypeId)
        {
            List<Tbl_Product_Sub_Types> subTypes = productRepo.getProductSubType(TypeId);
            return Json(subTypes, JsonRequestBehavior.AllowGet);
        }

        //Product Management Starts

        public ActionResult AddProducts()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.SuperCategories = productRepo.getSuperCategories();
                ViewBag.ProductTypes = productRepo.getProductTypes();
                return View();
            }
        }

        [HttpPost]
        public ActionResult AddProductInfo(ProductInventory productData, List<HttpPostedFileBase> image_file_arr, string Brand_Name, string flavoredProducts, string relatedProducts)
        {
            List<string> imagePaths = new List<string>();
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                if (image_file_arr != null && image_file_arr.Count() > 1)
                {
                    foreach (HttpPostedFileBase fileInfo in image_file_arr)
                    {
                        if (fileInfo.FileName != null)
                        {
                            var fileName = Path.GetFileName(fileInfo.FileName);
                            var path = Path.Combine(Server.MapPath("~/Images/Product"), fileName);
                            fileInfo.SaveAs(path);
                            imagePaths.Add("../Images/Product/" + fileName);
                        }
                    }
                }

                int status = productRepo.AddProductInfo(productData, imagePaths, Brand_Name, userData, flavoredProducts, relatedProducts);
                if (status == 0)
                {
                    ViewBag.ProductStatus = "Error";
                }
                else
                {
                    ViewBag.ProductStatus = "Success";
                }
                return RedirectToAction("AddProducts", "Home");
            }
        }

        public ActionResult SearchProducts()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Tbl_Product_Info> productsList = productRepo.getAllProducts();
                return View(productsList);
            }
        }

        public ActionResult EditProduct(long Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.SuperCategories = productRepo.getSuperCategories();
                ViewBag.ProductTypes = productRepo.getProductTypes();
                ProductInfo product = productRepo.getProductDetails(Id);
                return View(product);
            }
        }

        [HttpPost]
        public ActionResult EditProductInfo(ProductInventory productData, List<HttpPostedFileBase> image_file_arr, string Brand_Name, string flavoredProducts, string relatedProducts)
        {
            List<string> imagePaths = new List<string>();
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                if (image_file_arr.Count() <= 1)
                {

                }
                else
                {
                    foreach (HttpPostedFileBase fileInfo in image_file_arr)
                    {
                        if (fileInfo == null)
                        {

                        }
                        else
                        {
                            if (fileInfo.FileName != null)
                            {
                                var fileName = Path.GetFileName(fileInfo.FileName);
                                var path = Path.Combine(Server.MapPath("~/Images/Product"), fileName);
                                fileInfo.SaveAs(path);
                                imagePaths.Add("../Images/Product/" + fileName);
                            }
                        }
                    }
                }

                int status = productRepo.EditProductInfo(productData, imagePaths, Brand_Name, userData, flavoredProducts, relatedProducts);
                if (status == 0)
                {
                    ViewBag.ProductStatus = "Error";
                }
                else
                {
                    ViewBag.ProductStatus = "Success";
                }
                return RedirectToAction("SearchProducts", "Home");
            }
        }

        public JsonResult RemoveSubProduct(long subProductId)
        {
            int i = productRepo.RemoveSubProduct(subProductId);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchAllBrandList(string sord)
        {
            List<Def_Brand_Info> brandsInfo = productRepo.SearchAllBrandList(sord);
            return Json(brandsInfo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchAllGenericNameList(string sord)
        {
            List<Def_Generic_Names> genericNamesInfo = productRepo.SearchAllGenericList(sord);
            return Json(genericNamesInfo, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SearchAllProductList(string sord)
        {
            List<string> produtsName = productRepo.SearchAllProductList(sord);
            return Json(produtsName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchAutoCompleteProductList(string sord)
        {
            List<Tbl_Product_Info> produtsName = productRepo.SearchAutoCompleteProductList(sord);
            return Json(produtsName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getProudtcGallery(long productId)
        {
            string ProductImagePath = productRepo.getProudtcGallery(productId);
            return Json(ProductImagePath, JsonRequestBehavior.AllowGet);
        }

        //Product Management Ends

        //Package Management Starts

        public ActionResult AddPackage()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult AddPackageInfo(PackageInventory packageInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int status = 0;
                if (string.IsNullOrEmpty(packageInfo.Package_Name))
                {
                    ViewBag.PackageStatus = "Package Name Required...";
                }
                else
                {
                    status = productRepo.AddPackage(packageInfo, userData);
                }
                if (status == 1)
                {
                    ViewBag.PackageStatus = "Success";
                }
                else
                {
                    ViewBag.PackageStatus = "Error";
                }
                return RedirectToAction("AddPackage", "Home");
            }
        }

        public ActionResult SearchPackages()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Tbl_Packages_Info> packagesList = productRepo.getAllPackages();
                return View(packagesList);
            }
        }

        public ActionResult EditPackage(long packageId)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                PackageInfo packageInfo = productRepo.GetPackageDetails(packageId);
                return View(packageInfo);
            }
        }

        [HttpPost]
        public ActionResult EditPackageInfo(PackageInventory packageInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int status = productRepo.EditPackageInfo(packageInfo);
                return RedirectToAction("EditPackage", "Home", new { packageId = packageInfo.Package_Id });
            }
        }

        //Package Management Ends

        //Order Management starts

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult GetSearchOrders(long? OrderId, string TxnStatus, string PaymentMode, int? orderCurrentStatus, DateTime? FromDate, DateTime? ToDate, string ShipmentId, string CourierName, string PromoCode)
        {
            List<OrdersList> orders = new List<OrdersList>();

            orderCurrentStatus = orderCurrentStatus ?? 0;
            {
                orders = productRepo.getPartialOrders(OrderId, TxnStatus, PaymentMode, (int)orderCurrentStatus, FromDate, ToDate, ShipmentId, CourierName, PromoCode);
                return PartialView("_SearchOrders", orders);
            }

        }

        public ActionResult SearchOrders()
        {
            string PromoCode = Request.QueryString["PromoCode"];

            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!string.IsNullOrEmpty(PromoCode) && PromoCode.Length > 0)
            {
                List<OrdersList> orders = productRepo.getPartialOrders(0, "", "", 0, null, null, "", "", PromoCode);
                Def_Order_Current_Status orderCurrentStatus = new Def_Order_Current_Status();
                ViewBag.orderCurrentStatus = productRepo.getOrderCurrentStatusTypes();

                return View(orders);

            }
            else
            {
                List<OrdersList> orders = productRepo.getOrders();

                Def_Order_Current_Status orderCurrentStatus = new Def_Order_Current_Status();
                ViewBag.orderCurrentStatus = productRepo.getOrderCurrentStatusTypes();

                return View(orders);
            }
        }

        public ActionResult AuthorizedOrders()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<OrdersList> orders = productRepo.getAuthorizedOrders();
                return View(orders);
            }
        }

        public ActionResult PickupOrders()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<OrdersList> orders = productRepo.getPickupOrders();
                return View(orders);
            }
        }

        public ActionResult DispatchedOrders()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<OrdersList> orders = productRepo.getDispatchedOrders();
                return View(orders);
            }
        }

        public ActionResult DeliveredOrders()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<OrdersList> orders = productRepo.getDeliveredOrders();
                return View(orders);
            }
        }

        public JsonResult CreateShipment(string Ids, string courierName)
        {
            int status = productRepo.CreateShipment(Ids, courierName);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MoveToDispatchedOrders(string Ids, string pickupId, string shipmentId)
        {
            int status = productRepo.MoveToDispatchedOrders(Ids, pickupId, shipmentId);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MoveToDeliveredOrders(string Ids, string courierBoyName, string courierBoyNumber)
        {
            int status = productRepo.MoveToDeliveredOrders(Ids, courierBoyName, courierBoyNumber);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Orderdetails(long Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                OrderDetails orderInfo = productRepo.getOrderInfo(Id);
                return View(orderInfo);
            }
        }

        public JsonResult CancelOrder(long orderId)
        {
            int i = productRepo.CancelOrder(orderId);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        //Order Management Ends

        //User Management Starts

        public ActionResult UsersList()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Tbl_User_Info> usersList = productRepo.getAllUsers();
                return View(usersList);
            }
        }

        public ActionResult UserOrders(long Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<OrdersList> orders = productRepo.getUserOrders(Id);
                return View(orders);
            }
        }

        public ActionResult UserWallet(long Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<TBL_Wallet_Transactions> wallet = productRepo.getUserWallet(Id);
                return View(wallet);
            }
        }

        //User Management Ends

        //Inventory Management Starts

        public ActionResult SuperCategoryInventory()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Def_Super_Categories> superCategories = productRepo.getAllSuperCategories();
                return View(superCategories);
            }
        }

        public ActionResult CategoryInventory()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.SuperCategories = productRepo.getSuperCategories();
                List<CategoryList> Categories = productRepo.getAllCategories();
                return View(Categories);
            }
        }

        public ActionResult SubCategoryInventory()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.SuperCategories = productRepo.getSuperCategories();
                List<SubCategoriesList> Categories = productRepo.getAllSubCategories();
                return View(Categories);
            }
        }

        public ActionResult ProductTypeInventory()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Def_Product_Type> productTypes = productRepo.getAllProductTypes();
                return View(productTypes);
            }
        }

        public ActionResult ProductSubTypeInventory()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.ProductTypes = productRepo.getProductTypes();
                List<ProductSubTypeList> subTypes = productRepo.getAllProductSubTypes();
                return View(subTypes);
            }
        }

        [HttpPost]
        public ActionResult AddSuperCategory(Def_Super_Categories superCatInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.AddSuperCategory(superCatInfo, userData);
                if (i == -1)
                {
                    ViewBag.SuperCatInfo = "Supercategory alreay exist";
                }
                else if (i == 0)
                {
                    ViewBag.SuperCatInfo = "Something wrong with adding Supercategory";
                }
                else if (i == 1)
                {
                    ViewBag.SuperCatInfo = "Supercategory added successfully";
                }
                return RedirectToAction("SuperCategoryInventory", "Home");
            }
        }

        [HttpPost]
        public ActionResult AddCategory(Def_Categories catInfo)
        {
            if (catInfo.Super_Category_Id == 0 || catInfo.Super_Category_Id == null)
            {
                ViewBag.CatInfo = "Select Supercategory...";
            }
            else
            {
                Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
                if (userData == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    int i = productRepo.AddCategory(catInfo, userData);
                    if (i == -1)
                    {
                        ViewBag.CatInfo = "Category alreay exist";
                    }
                    else if (i == 0)
                    {
                        ViewBag.CatInfo = "Something wrong with adding Category";
                    }
                    else if (i == 1)
                    {
                        ViewBag.CatInfo = "Category added successfully";
                    }
                }
            }
            return RedirectToAction("CategoryInventory", "Home");

        }

        [HttpPost]
        public ActionResult AddSubCategory(Def_Sub_Categories subCatInfo)
        {
            if (subCatInfo.Category_Id == 0 || subCatInfo.Category_Id == null)
            {
                ViewBag.SubCatInfo = "Select Category...";
            }
            else
            {
                Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
                if (userData == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    int i = productRepo.AddSubCategory(subCatInfo, userData);
                    if (i == -1)
                    {
                        ViewBag.SubCatInfo = "Subcategory alreay exist";
                    }
                    else if (i == 0)
                    {
                        ViewBag.SubCatInfo = "Something wrong with adding Subcategory";
                    }
                    else if (i == 1)
                    {
                        ViewBag.SubCatInfo = "Subcategory added successfully";
                    }
                }
            }
            return RedirectToAction("SubCategoryInventory", "Home");
        }

        [HttpPost]
        public ActionResult AddProductType(Def_Product_Type typeInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.AddProductType(typeInfo, userData);
                if (i == -1)
                {
                    ViewBag.ProductTypeInfo = "ProductType alreay exist";
                }
                else if (i == 0)
                {
                    ViewBag.ProductTypeInfo = "Something wrong with adding ProductType";
                }
                else if (i == 1)
                {
                    ViewBag.ProductTypeInfo = "ProductType added successfully";
                }
                return RedirectToAction("ProductTypeInventory", "Home");
            }
        }

        [HttpPost]
        public ActionResult AddProductSubType(Tbl_Product_Sub_Types subTypeInfo)
        {
            if (subTypeInfo.Product_Type_Id == 0 || subTypeInfo.Product_Type_Id == null)
            {
                ViewBag.CatInfo = "Select Product Type...";
            }
            else
            {
                Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
                if (userData == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    int i = productRepo.AddProductSubType(subTypeInfo, userData);
                    if (i == -1)
                    {
                        ViewBag.CatInfo = "Product Sub Type alreay exist";
                    }
                    else if (i == 0)
                    {
                        ViewBag.CatInfo = "Something wrong with adding Product Sub Type ";
                    }
                    else if (i == 1)
                    {
                        ViewBag.CatInfo = "Product Sub Type  added successfully";
                    }
                }
            }
            return RedirectToAction("ProductSubTypeInventory", "Home");
        }

        public ActionResult EditSuperCategory(int Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Def_Super_Categories> super_Categories = productRepo.getSuperCategories();
                return View(super_Categories.Where(x => x.Super_Category_Id == Id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult EditSuperCategory(Def_Super_Categories supCatInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.EditSuperCategoryInfo(supCatInfo, userData);
                return RedirectToAction("SuperCategoryInventory", "Home");
            }
        }

        public ActionResult EditCategory(int Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.SuperCategories = productRepo.getSuperCategories();
                Def_Categories CategoryInfo = productRepo.getCategoryByCategoryId(Id);
                return View(CategoryInfo);
            }
        }

        [HttpPost]
        public ActionResult EditCategory(Def_Categories catInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.EditCategoryInfo(catInfo, userData);
                return RedirectToAction("CategoryInventory", "Home");
            }
        }

        public ActionResult EditSubCategory(long Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.SuperCategories = productRepo.getSuperCategories();
                SubCategoriesList SubCategoryInfo = productRepo.getSubCategoryBySubCategoryId(Id);
                ViewBag.Categories = productRepo.getCategoriesBySuperCategory(SubCategoryInfo.SuperCategoryId);
                return View(SubCategoryInfo);
            }
        }

        [HttpPost]
        public ActionResult EditSubCategory(Def_Sub_Categories subCatInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.EditSubCategoryInfo(subCatInfo, userData);
                return RedirectToAction("SubCategoryInventory", "Home");
            }
        }

        public ActionResult EditProductType(int Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Def_Product_Type> productTypes = productRepo.getProductTypes();
                return View(productTypes.Where(x => x.Product_Type_Id == Id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult EditProductType(Def_Product_Type productTypeInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.EditProductTypeInfo(productTypeInfo, userData);
                return RedirectToAction("ProductTypeInventory", "Home");
            }
        }

        public ActionResult EditProductSubType(int Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.ProductTypes = productRepo.getProductTypes();
                Tbl_Product_Sub_Types productSubTypesInfo = productRepo.getSubTypesBySubTypeId(Id);
                return View(productSubTypesInfo);
            }
        }

        [HttpPost]
        public ActionResult EditProductSubType(Tbl_Product_Sub_Types subTypeInfo)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.EditProductSubTypeInfo(subTypeInfo, userData);
                return RedirectToAction("ProductSubTypeInventory", "Home");
            }
        }

        public JsonResult getSuperCategoryProductsCount(int superCatId)
        {
            int productCount = productRepo.getSuperCategoryProductsCount(superCatId);
            return Json(productCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeSuperCategoryStatus(int superCatId)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            int i = 0;
            if (userData != null)
            {
                i = productRepo.ChangeSuperCategoryStatus(superCatId, userData);
            }
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCategoryProductsCount(int catId)
        {
            int productCount = productRepo.getCategoryProductsCount(catId);
            return Json(productCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeCategoryStatus(int catId)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            int i = 0;
            if (userData != null)
            {
                i = productRepo.ChangeCategoryStatus(catId, userData);
            }
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSubCategoryProductsCount(int subCatId)
        {
            int productCount = productRepo.getSubCategoryProductsCount(subCatId);
            return Json(productCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeSubCategoryStatus(int subCatId)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            int i = 0;
            if (userData != null)
            {
                i = productRepo.ChangeSubCategoryStatus(subCatId, userData);
            }
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getProductTypeProductsCount(int typeId)
        {
            int productCount = productRepo.getProductTypeProductsCount(typeId);
            return Json(productCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeProductTypeStatus(int typeId)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            int i = 0;
            if (userData != null)
            {
                i = productRepo.ChangeProductTypeStatus(typeId, userData);
            }
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getProductSubTypeProductsCount(int subtypeId)
        {
            int productCount = productRepo.getProductSubTypeProductsCount(subtypeId);
            return Json(productCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeProductSubTypeStatus(int subtypeId)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            int i = 0;
            if (userData != null)
            {
                i = productRepo.ChangeProductSubTypeStatus(subtypeId, userData);
            }
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        //Inventory Management Ends

        //Banner Management Starts

        public ActionResult AddBanners()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.BannerInfo = productRepo.getBannerRelatedInfo();
                return View();
            }
        }

        public JsonResult getBannerValue(int BannerPageId)
        {
            if (BannerPageId == 1)
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            else if (BannerPageId == 2)
            {
                List<Def_Super_Categories> superCatInfo = productRepo.getSuperCategories();
                return Json(superCatInfo, JsonRequestBehavior.AllowGet);
            }
            else if (BannerPageId == 3)
            {
                List<CategoryList> CatInfo = productRepo.getAllCategories();
                return Json(CatInfo, JsonRequestBehavior.AllowGet);
            }
            else if (BannerPageId == 4)
            {
                List<SubCategoriesList> subCatInfo = productRepo.getAllSubCategories();
                return Json(subCatInfo, JsonRequestBehavior.AllowGet);
            }
            else if (BannerPageId == 5)
            {
                List<Def_Brand_Info> brandInfo = productRepo.getAllBrands();
                return Json(brandInfo, JsonRequestBehavior.AllowGet);
            }
            else if (BannerPageId == 6)
            {
                List<Tbl_Packages_Info> PackageInfo = productRepo.getAllPackages();
                return Json(PackageInfo, JsonRequestBehavior.AllowGet);
            }
            else if (BannerPageId == 7)
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddBannerInfo(List<HttpPostedFileBase> image_file_arr, Tbl_Banner_Gallery_Info bannerGallery)
        {
            List<string> imagePaths = new List<string>();
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                if (image_file_arr.Count() < 1)
                {

                }
                else
                {
                    foreach (HttpPostedFileBase fileInfo in image_file_arr)
                    {
                        if (fileInfo.FileName != null)
                        {
                            var fileName = Path.GetFileName(fileInfo.FileName);
                            var path = Path.Combine(Server.MapPath("~/Images/Banner"), fileName);
                            fileInfo.SaveAs(path);
                            imagePaths.Add("../Images/Banner/" + fileName);
                        }
                    }
                }

                int status = productRepo.AddBannerInfo(bannerGallery, imagePaths, userData);
                if (status == 0)
                {
                    ViewBag.BannerStatus = "Error";
                }
                else
                {
                    ViewBag.BannerStatus = "Success";
                }
                return RedirectToAction("AddBanners", "Home");
            }
        }

        public ActionResult SearchBanners()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<BannersInfo> banners = productRepo.getBannersInfo();
                return View(banners);
            }
        }

        public JsonResult DeleteBanner(long BannerId)
        {
            int i = 0;
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
            }
            else
            {
                i = productRepo.DeleteBanner(BannerId, userData);
            }
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditBanner(long Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                ViewBag.BannerInfo = productRepo.getBannerRelatedInfo();
                List<BannersInfo> bannerInfo = productRepo.getBannersInfo();
                return View(bannerInfo.Where(x => x.BannerId == Id).FirstOrDefault());
            }
        }
        [HttpPost]
        public ActionResult EditBannerInfo(Tbl_Banner_Gallery_Info bannerGallery, HttpPostedFileBase image_file_arr)
        {
            List<string> imagePaths = new List<string>();
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                if (image_file_arr == null)
                {

                }
                else
                {
                    if (image_file_arr.FileName != null)
                    {
                        var fileName = Path.GetFileName(image_file_arr.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/Banner"), fileName);
                        image_file_arr.SaveAs(path);
                        imagePaths.Add("../Images/Banner/" + fileName);
                    }
                }

                int status = productRepo.EditBannerInfo(bannerGallery, imagePaths, userData);
                if (status == 0)
                {
                    ViewBag.BannerStatus = "Error";
                }
                else
                {
                    ViewBag.BannerStatus = "Success";
                }
            }

            return RedirectToAction("EditBanner", "Home", new { Id = bannerGallery.Banner_Id });
        }

        //Banner Management Ends

        //Notify Me Products
        public ActionResult NotifyMeProducts()
        {
            List<NotifyProducts> notify = productRepo.NotifyProducts();
            return View(notify);
        }

        //Notify Me end here

        /// <summary>
        /// Coupon Management Code
        /// </summary>

        public ActionResult AddCoupon()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                Def_Coupon_Discount_Types couponDiscountTypes = new Def_Coupon_Discount_Types();
                ViewBag.CouponDiscountTypes = productRepo.getCouponDiscountTypes();

                return View();
            }
        }

        [HttpPost]
        public ActionResult AddCoupon(Tbl_Coupon_Info couponData)
        {
            Tbl_User_Info AdminInfo = (Tbl_User_Info)Session["login"];
            if (AdminInfo == null)
            {
                return RedirectToAction("Login");
            }
            else
            {
                int status = productRepo.AddCoupon(couponData);
                if (status == 0)
                {
                    TempData["CouponError"] = "Error";
                }
                else
                {
                    TempData["CouponError"] = "Success";
                }
                return RedirectToAction("AddCoupon", "Home");
            }
        }

        public ActionResult SearchCoupons()
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];
            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Tbl_Coupon_Info> couponsList = productRepo.getAllCoupons();
                return View(couponsList);
            }
        }

        public ActionResult DisableCoupons(long Coupon_Id)
        {
            Tbl_User_Info userData = (Tbl_User_Info)Session["login"];

            if (userData == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int i = productRepo.DisableCouponCode(Coupon_Id);
                return RedirectToAction("SearchCoupons");
            }
        }

        /// <summary>
        /// Coupon Management Code End
        /// </summary>

    }
}