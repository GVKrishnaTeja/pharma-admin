﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace BAL
{
    public class ProductRepository : IProductRepository
    {

        Db_Pharma_MartEntities context = new Db_Pharma_MartEntities();

        public int AddPackage(PackageInventory packageInfo, Tbl_User_Info userData)
        {
            int i = 0;
            int resultTblPackages = 0;
            try
            {

                Tbl_Packages_Info package = new Tbl_Packages_Info();
                if (packageInfo.Package_Type_Id == 1)
                {
                    package.Is_Customized = false;
                }
                else
                {
                    package.Is_Customized = true;
                }
                package.Max_Products_Count = packageInfo.packageProducts.Split(',').Count();
                package.Package_Actual_Cost = packageInfo.Actual_Cost;
                package.Package_Description = packageInfo.package_Description;
                package.Package_Discount_Percentage = packageInfo.Discount_Percentage;
                package.Package_Expiry_Date = packageInfo.Package_Expiry;
                package.Package_Final_Cost = packageInfo.Final_Cost;
                package.Package_Name = packageInfo.Package_Name;
                package.Package_Type_Id = packageInfo.Package_Type_Id;
                package.Package_Quantity = packageInfo.package_Quantity;
                package.Is_Active = true;
                package.Is_Deleted = false;
                package.Updated_By = userData.User_Id;
                package.Created_By = userData.User_Id;
                package.Created_Date = DateTime.Now;
                package.Updated_Date = DateTime.Now;
                package.Package_Min_Amount = packageInfo.package_Min_Amount;
                package.Package_Min_Products_Count = packageInfo.package_Min_Products_Count;
                context.Tbl_Packages_Info.Add(package);
                resultTblPackages = context.SaveChanges();

                var customPackageId = package.Package_Id;

                int j = 0;
                foreach (string productId in packageInfo.packageProducts.Split(','))
                {
                    Tbl_Package_Products product = new Tbl_Package_Products();
                    product.Package_Id = package.Package_Id;
                    product.Product_Id = Convert.ToInt64(productId);
                    product.Package_Product_Quantity = packageInfo.Product_quantity[j];
                    product.Is_Active = true;
                    product.Is_Deleted = false;
                    product.Updated_By = userData.User_Id;
                    product.Created_By = userData.User_Id;
                    product.Created_Date = DateTime.Now;
                    product.Updated_Date = DateTime.Now;
                    context.Tbl_Package_Products.Add(product);
                    i = context.SaveChanges();
                    j++;
                }

                //Insert for the customized packages as cashback(promocode).
                if (resultTblPackages > 0 && customPackageId > 0)
                {
                    Tbl_Coupon_Info coupon = new Tbl_Coupon_Info()
                    {
                        Coupon_Code = "CPKG_" + customPackageId,
                        Valid_From = DateTime.Now,
                        //Valid_To = couponData.Valid_To,
                        Min_Cart_Value = packageInfo.package_Min_Amount,
                        //Max_Discount_Value = couponData.Max_Discount_Value,
                        Coupon_Discount_Type_Id = 2,
                        Coupon_Discount_Type_Value = packageInfo.CashBack_Percentage,
                        Created_Date = DateTime.Now,
                        Status = true,
                    };

                    context.Tbl_Coupon_Info.Add(coupon);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int AddProductInfo(ProductInventory productData, List<string> imagePaths, string brand_Name, Tbl_User_Info userData, string flavoredProducts, string relatedProducts)
        {
            int i = 0;
            try
            {
                long brand_Id = 0;
                long generic_Id = 0;

                if (context.Tbl_Product_Info.Where(x => x.Product_Name.Contains(productData.Product_Name)).Count() == 0)
                {
                    if (context.Def_Brand_Info.Where(x => x.Brand_Name.Contains(brand_Name)).Count() == 0)
                    {
                        Def_Brand_Info brandInfo = new Def_Brand_Info()
                        {
                            Brand_Name = brand_Name,
                            Is_Active = true,
                            Is_Deleted = false,
                            Updated_By = userData.User_Id,
                            Created_By = userData.User_Id,
                            Created_Date = DateTime.Now,
                            Updated_Date = DateTime.Now
                        };
                        context.Def_Brand_Info.Add(brandInfo);
                        context.SaveChanges();
                        brand_Id = brandInfo.Brand_Id;
                    }
                    else
                    {
                        brand_Id = context.Def_Brand_Info.Where(x => x.Brand_Name.Contains(brand_Name)).FirstOrDefault().Brand_Id;
                    }

                    if (context.Def_Generic_Names.Where(x => x.Generic_Name.Contains(productData.Generic_Name)).Count() == 0)
                    {
                        Def_Generic_Names genericInfo = new Def_Generic_Names()
                        {
                            Generic_Name = productData.Generic_Name,
                            Is_Active = true,
                            Is_Deleted = false,
                            Updated_By = userData.User_Id,
                            Created_By = userData.User_Id,
                            Created_Date = DateTime.Now,
                            Updated_Date = DateTime.Now
                        };
                        context.Def_Generic_Names.Add(genericInfo);
                        context.SaveChanges();
                        generic_Id = genericInfo.Generic_Id;
                    }
                    else
                    {
                        generic_Id = context.Def_Generic_Names.Where(x => x.Generic_Name.Contains(productData.Generic_Name)).FirstOrDefault().Generic_Id;
                    }

                    Tbl_Product_Info newProduct = new Tbl_Product_Info()
                    {
                        Actual_Cost = productData.Actual_Cost,
                        Brand_Id = brand_Id,
                        Discount_Amount = (productData.Actual_Cost - productData.Final_Cost),
                        Final_Cost = productData.Final_Cost,
                        Discount_Percentage = productData.Discount_Percentage,
                        Features = productData.Features,
                        Full_Description = productData.Full_Description,
                        H1_Tag = productData.H1_Tag,
                        Is_Featured = productData.Is_Featured,
                        Is_Prescription_Needed = productData.Is_Prescription_Needed,
                        Is_Sold = productData.Is_Sold,
                        Meta_Description = productData.Meta_Description,
                        Meta_Keywords = productData.Meta_Keywords,
                        Page_Title = productData.Page_Title,
                        Product_Code = productData.Product_Code,
                        Product_Name = productData.Product_Name,
                        Product_Sub_Type_Id = productData.Product_Sub_Type_Id,
                        Quantity = productData.Quantity,
                        Shipping_Cost = productData.Shipping_Cost,
                        Short_Description = productData.Short_Description,
                        Sub_Category_Id = productData.Sub_Category_Id,
                        Is_Active = true,
                        Is_Deleted = false,
                        Updated_By = userData.User_Id,
                        Created_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now,
                        Generic_Id = generic_Id,
                        Have_SubProducts = productData.Have_SubProducts,
                        Product_Sub_Type_Inventory = productData.Product_Sub_Type_Inventory
                    };

                    context.Tbl_Product_Info.Add(newProduct);
                    i = context.SaveChanges();
                    productData.Product_Id = newProduct.Product_Id;
                    if (productData.Have_SubProducts == true)
                    {
                        foreach (SubProductInventory subProuctInfo in productData.subProductInfo)
                        {
                            Tbl_Sub_Product_Info subProduct = new Tbl_Sub_Product_Info()
                            {
                                Actual_Cost = subProuctInfo.Actual_Cost,
                                Created_By = userData.User_Id,
                                Created_Date = DateTime.Now,
                                Is_Active = true,
                                Is_Deleted = false,
                                Pack_Of_Products_Count = subProuctInfo.Pack_Of_Products_Count,
                                Pack_Of_Products_Discount_Percentage = subProuctInfo.Pack_Of_Products_Discount_Percentage,
                                Pack_Of_Products_Final_Cost = subProuctInfo.Pack_Of_Products_Final_Cost,
                                Product_Id = productData.Product_Id,
                                Product_Sub_Type_Id = subProuctInfo.Product_Sub_Type_Id,
                                Updated_By = userData.User_Id,
                                Updated_Date = DateTime.Now,
                                Quantity = subProuctInfo.Sub_Product_Quantity,
                                Product_Sub_Type_Inventory = subProuctInfo.Product_Sub_Type_Inventory
                            };
                            context.Tbl_Sub_Product_Info.Add(subProduct);
                            context.SaveChanges();
                        }
                    }

                    if (string.IsNullOrEmpty(flavoredProducts))
                    {

                    }
                    else
                    {
                        string[] productsFlavored = flavoredProducts.Split(',');
                        foreach (string pro in productsFlavored)
                        {
                            if (string.IsNullOrEmpty(pro))
                            {

                            }
                            else
                            {
                                string productName = pro.Replace("-", " ");
                                Tbl_Product_Info flavoredproduct = context.Tbl_Product_Info.Where(x => x.Product_Name == productName).FirstOrDefault();
                                if (flavoredproduct != null)
                                {
                                    Tbl_Flavoured_Products flavored = new Tbl_Flavoured_Products()
                                    {
                                        Product_Id = productData.Product_Id,
                                        Created_On = DateTime.Now,
                                        Flavoured_Product_Id = flavoredproduct.Product_Id,
                                        Is_Active = true,
                                        Is_Deleted = false,
                                        Updated_On = DateTime.Now
                                    };
                                    context.Tbl_Flavoured_Products.Add(flavored);
                                    context.SaveChanges();
                                }

                            }
                        }
                    }

                    if (string.IsNullOrEmpty(relatedProducts))
                    {

                    }
                    else
                    {
                        string[] productsRelated = relatedProducts.Split(',');
                        foreach (string pro in productsRelated)
                        {
                            if (string.IsNullOrEmpty(pro))
                            {

                            }
                            else
                            {
                                string productName = pro.Replace("-", " ");
                                Tbl_Product_Info relatedProduct = context.Tbl_Product_Info.Where(x => x.Product_Name == productName).FirstOrDefault();
                                if (relatedProduct != null)
                                {
                                    Tbl_Related_Products related = new Tbl_Related_Products()
                                    {
                                        Product_Id = productData.Product_Id,
                                        Created_Date = DateTime.Now,
                                        Related_Product_Id = relatedProduct.Product_Id,
                                        Is_Active = true,
                                        Is_Deleted = false,
                                        Updated_Date = DateTime.Now
                                    };
                                    context.Tbl_Related_Products.Add(related);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }

                    if (imagePaths.Count() != 0)
                    {
                        foreach (string path in imagePaths)
                        {
                            Tbl_Product_Gallery gallery = new Tbl_Product_Gallery()
                            {
                                Img_Url = path,
                                Large_Image_Url = path,
                                Product_Id = productData.Product_Id,
                                Is_Active = true,
                                Is_Deleted = false,
                                Updated_By = userData.User_Id,
                                Created_By = userData.User_Id,
                                Created_Date = DateTime.Now,
                                Updated_Date = DateTime.Now
                            };
                            context.Tbl_Product_Gallery.Add(gallery);
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int EditPackageInfo(PackageInventory packageInfo)
        {
            int i = 0;
            try
            {
                Tbl_Packages_Info packageData = context.Tbl_Packages_Info.Where(x => x.Package_Id == packageInfo.Package_Id).FirstOrDefault();
                if (packageInfo.Package_Type_Id == 1)
                {
                    packageData.Is_Customized = false;
                }
                else if (packageInfo.Package_Type_Id == 2)
                {
                    packageData.Is_Customized = true;
                }
                packageData.Max_Products_Count = packageInfo.packageProducts.Split(',').Count();
                packageData.Package_Actual_Cost = packageInfo.Actual_Cost;
                packageData.Package_Description = packageInfo.package_Description;
                packageData.Package_Discount_Percentage = packageInfo.Discount_Percentage;
                packageData.Package_Expiry_Date = packageInfo.Package_Expiry;
                packageData.Package_Final_Cost = packageInfo.Final_Cost;
                packageData.Package_Name = packageInfo.Package_Name;
                packageData.Package_Quantity = packageInfo.package_Quantity;
                packageData.Package_Type_Id = packageInfo.Package_Type_Id;
                packageData.Package_Min_Amount = packageInfo.package_Min_Amount;
                packageData.Package_Min_Products_Count = packageInfo.package_Min_Products_Count;
                context.SaveChanges();
                int j = 0;
                foreach (string productId in packageInfo.packageProducts.Split(','))
                {
                    long Id = Convert.ToInt64(productId);
                    if (context.Tbl_Package_Products.Where(x => x.Product_Id == Id && x.Package_Id == packageInfo.Package_Id).Count() > 0)
                    {
                        Tbl_Package_Products product = context.Tbl_Package_Products.Where(x => x.Product_Id == Id && x.Package_Id == packageInfo.Package_Id).FirstOrDefault();
                        product.Package_Product_Quantity = packageInfo.Product_quantity[j];
                        i = context.SaveChanges();
                    }
                    else
                    {
                        Tbl_Package_Products product = new Tbl_Package_Products();
                        product.Package_Id = packageInfo.Package_Id;
                        product.Product_Id = Convert.ToInt64(productId);
                        product.Updated_Date = DateTime.Now;
                        product.Package_Product_Quantity = packageInfo.Product_quantity[j];
                        context.Tbl_Package_Products.Add(product);
                        i = context.SaveChanges();
                    }
                    j++;
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int EditProductInfo(ProductInventory productData, List<string> imagePaths, string brand_Name, Tbl_User_Info userData, string flavoredProducts, string relatedProducts)
        {
            int i = 0;
            try
            {
                long brand_Id = 0;
                long generic_Id = 0;
                long status_Id = 0;

                if (context.Def_Brand_Info.Where(x => x.Brand_Name.Contains(brand_Name)).Count() == 0)
                {
                    Def_Brand_Info brandInfo = new Def_Brand_Info()
                    {
                        Brand_Name = brand_Name,
                        Is_Active = true,
                        Is_Deleted = false,
                        Updated_By = userData.User_Id,
                        Created_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now
                    };
                    context.SaveChanges();
                    brand_Id = brandInfo.Brand_Id;
                }
                else
                {
                    brand_Id = context.Def_Brand_Info.Where(x => x.Brand_Name.Contains(brand_Name)).FirstOrDefault().Brand_Id;
                }

                if (context.Def_Generic_Names.Where(x => x.Generic_Name.Contains(productData.Generic_Name)).Count() == 0)
                {
                    Def_Generic_Names genericInfo = new Def_Generic_Names()
                    {
                        Generic_Name = productData.Generic_Name,
                        Is_Active = true,
                        Is_Deleted = false,
                        Updated_By = userData.User_Id,
                        Created_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now
                    };
                    context.Def_Generic_Names.Add(genericInfo);
                    context.SaveChanges();
                    generic_Id = genericInfo.Generic_Id;
                }
                else
                {
                    generic_Id = context.Def_Generic_Names.Where(x => x.Generic_Name.Contains(productData.Generic_Name)).FirstOrDefault().Generic_Id;
                }

                Tbl_Product_Info editProduct = context.Tbl_Product_Info.Where(x => x.Product_Id == productData.Product_Id).FirstOrDefault();

                editProduct.Actual_Cost = productData.Actual_Cost;
                editProduct.Brand_Id = brand_Id;
                editProduct.Discount_Amount = (productData.Actual_Cost - productData.Final_Cost);
                editProduct.Final_Cost = productData.Final_Cost;
                editProduct.Discount_Percentage = productData.Discount_Percentage;
                editProduct.Features = productData.Features;
                editProduct.Full_Description = productData.Full_Description;
                editProduct.H1_Tag = productData.H1_Tag;
                editProduct.Is_Featured = productData.Is_Featured;
                editProduct.Is_Prescription_Needed = productData.Is_Prescription_Needed;
                editProduct.Is_Sold = productData.Is_Sold;
                editProduct.Meta_Description = productData.Meta_Description;
                editProduct.Meta_Keywords = productData.Meta_Keywords;
                editProduct.Page_Title = productData.Page_Title;
                editProduct.Product_Code = productData.Product_Code;
                editProduct.Product_Name = productData.Product_Name;
                editProduct.Product_Sub_Type_Id = productData.Product_Sub_Type_Id;
                editProduct.Quantity = productData.Quantity;
                editProduct.Shipping_Cost = productData.Shipping_Cost;
                editProduct.Short_Description = productData.Short_Description;
                editProduct.Sub_Category_Id = productData.Sub_Category_Id;
                editProduct.Is_Active = true;
                editProduct.Is_Deleted = false;
                editProduct.Updated_By = userData.User_Id;
                editProduct.Created_By = userData.User_Id;
                editProduct.Created_Date = DateTime.Now;
                editProduct.Updated_Date = DateTime.Now;
                editProduct.Generic_Id = generic_Id;

                if (productData.Have_SubProducts == true)
                {
                    foreach (SubProductInventory sub in productData.subProductInfo)
                    {
                        if (sub.Sub_Product_Id == null || sub.Sub_Product_Id == 0)
                        {
                            Tbl_Sub_Product_Info subProduct = new Tbl_Sub_Product_Info()
                            {
                                Actual_Cost = sub.Actual_Cost,
                                Created_By = userData.User_Id,
                                Created_Date = DateTime.Now,
                                Is_Active = true,
                                Is_Deleted = false,
                                Pack_Of_Products_Count = sub.Pack_Of_Products_Count,
                                Pack_Of_Products_Discount_Percentage = sub.Pack_Of_Products_Discount_Percentage,
                                Pack_Of_Products_Final_Cost = sub.Pack_Of_Products_Final_Cost,
                                Product_Id = productData.Product_Id,
                                Product_Sub_Type_Id = sub.Product_Sub_Type_Id,
                                Product_Sub_Type_Inventory = sub.Product_Sub_Type_Inventory,
                                Quantity = sub.Sub_Product_Quantity,
                                Updated_By = userData.User_Id,
                                Updated_Date = DateTime.Now
                            };
                            context.Tbl_Sub_Product_Info.Add(subProduct);
                        }
                        else
                        {
                            Tbl_Sub_Product_Info subData = context.Tbl_Sub_Product_Info.Where(x => x.Sub_Product_Id == sub.Sub_Product_Id).FirstOrDefault();
                            subData.Actual_Cost = sub.Actual_Cost;
                            subData.Updated_By = userData.User_Id;
                            subData.Updated_Date = DateTime.Now;
                            subData.Is_Active = true;
                            subData.Is_Deleted = false;
                            subData.Pack_Of_Products_Count = sub.Pack_Of_Products_Count;
                            subData.Pack_Of_Products_Discount_Percentage = sub.Pack_Of_Products_Discount_Percentage;
                            subData.Pack_Of_Products_Final_Cost = sub.Pack_Of_Products_Final_Cost;
                            subData.Product_Id = productData.Product_Id;
                            subData.Product_Sub_Type_Id = sub.Product_Sub_Type_Id;
                            subData.Product_Sub_Type_Inventory = sub.Product_Sub_Type_Inventory;
                            subData.Quantity = sub.Sub_Product_Quantity;
                        }
                        i = context.SaveChanges();
                    }
                }

                if (string.IsNullOrEmpty(flavoredProducts))
                {

                }
                else
                {
                    string[] productsFlavored = flavoredProducts.Split(',');
                    foreach (string pro in productsFlavored)
                    {
                        if (string.IsNullOrEmpty(pro))
                        {

                        }
                        else
                        {
                            string productName = pro.Replace("-", " ");
                            Tbl_Product_Info flavoredproduct = context.Tbl_Product_Info.Where(x => x.Product_Name == productName).FirstOrDefault();
                            if (flavoredproduct != null)
                            {
                                Tbl_Flavoured_Products flavored = new Tbl_Flavoured_Products()
                                {
                                    Product_Id = productData.Product_Id,
                                    Created_On = DateTime.Now,
                                    Flavoured_Product_Id = flavoredproduct.Product_Id,
                                    Is_Active = true,
                                    Is_Deleted = false,
                                    Updated_On = DateTime.Now
                                };
                                context.Tbl_Flavoured_Products.Add(flavored);
                                context.SaveChanges();
                            }

                        }
                    }
                }


                if (string.IsNullOrEmpty(relatedProducts))
                {

                }
                else
                {
                    string[] productsRelated = relatedProducts.Split(',');
                    foreach (string pro in productsRelated)
                    {
                        if (string.IsNullOrEmpty(pro))
                        {

                        }
                        else
                        {
                            string productName = pro.Replace("-", " ");
                            Tbl_Product_Info relatedProduct = context.Tbl_Product_Info.Where(x => x.Product_Name == productName).FirstOrDefault();
                            if (relatedProduct != null)
                            {
                                Tbl_Related_Products related = new Tbl_Related_Products()
                                {
                                    Product_Id = productData.Product_Id,
                                    Created_Date = DateTime.Now,
                                    Related_Product_Id = relatedProduct.Product_Id,
                                    Is_Active = true,
                                    Is_Deleted = false,
                                    Updated_Date = DateTime.Now
                                };
                                context.Tbl_Related_Products.Add(related);
                                context.SaveChanges();
                            }
                        }
                    }
                }

                i = context.SaveChanges();

                if (imagePaths.Count() != 0)
                {
                    foreach (string path in imagePaths)
                    {
                        Tbl_Product_Gallery gallery = new Tbl_Product_Gallery()
                        {
                            Img_Url = path,
                            Large_Image_Url = path,
                            Product_Id = productData.Product_Id,
                            Is_Active = true,
                            Is_Deleted = false,
                            Updated_By = userData.User_Id,
                            Created_By = userData.User_Id,
                            Created_Date = DateTime.Now,
                            Updated_Date = DateTime.Now
                        };
                        context.Tbl_Product_Gallery.Add(gallery);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<Tbl_Packages_Info> getAllPackages()
        {
            List<Tbl_Packages_Info> packagesList = new List<Tbl_Packages_Info>();
            try
            {
                packagesList = (from package in context.Tbl_Packages_Info
                                where package.Is_Active == true && package.Is_Deleted == false
                                select package
                              ).ToList();
            }
            catch (Exception ex)
            {

            }
            return packagesList;
        }

        public List<Tbl_Product_Info> getAllProducts()
        {
            List<Tbl_Product_Info> productsList = new List<Tbl_Product_Info>();
            try
            {
                productsList = (from product in context.Tbl_Product_Info
                                where product.Is_Active == true && product.Is_Deleted == false
                                select product
                              ).ToList();
            }
            catch (Exception ex)
            {

            }
            return productsList;
        }

        public List<Tbl_Product_Info> getAllInActiveProducts()
        {
            List<Tbl_Product_Info> productsList = new List<Tbl_Product_Info>();
            try
            {
                productsList = (from product in context.Tbl_Product_Info
                                where product.Is_Active == false && product.Is_Deleted == false
                                select product
                              ).ToList();
            }
            catch (Exception ex)
            {

            }
            return productsList;
        }

        public List<OrdersList> getAuthorizedOrders()
        {
            List<OrdersList> orders = new List<OrdersList>();
            try
            {
                try
                {
                    orders = (from txn in context.Tbl_Payment_Transactions
                              join user in context.Tbl_User_Info
                              on txn.User_Id equals user.User_Id
                              join orderStatus in context.Def_Order_Current_Status
                              on txn.Order_Current_Status equals orderStatus.OrderCurrentStatusId
                              where txn.Authorized == true && (txn.Pickup == false || txn.Pickup == null) && (txn.Dispatched == false || txn.Dispatched == null) && (txn.Delivered == false || txn.Delivered == null)
                              && txn.Txn_Status == "SUCCESS" && txn.Order_Current_Status == 2
                              select new OrdersList
                              {
                                  paymentTransactionId = txn.PaymentTransaction_Id,
                                  PaymentMode = txn.Payment_Mode,
                                  TxnAmount = txn.Txn_Amount,
                                  TxnStatus = txn.Txn_Status,
                                  orderCurrentStatus = orderStatus.OrderCurrentStatus,
                                  TxnDate = txn.Txn_Date,
                                  userId = user.User_Id,
                                  userName = user.First_Name
                              }).ToList();

                    foreach (OrdersList singleOrder in orders)
                    {
                        List<Tbl_User_Payment_Transactions> userTxns = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == singleOrder.paymentTransactionId).ToList();
                        singleOrder.productId = new List<long?>();
                        singleOrder.productName = new List<string>();
                        singleOrder.packageId = new List<long?>();
                        singleOrder.packageName = new List<string>();
                        foreach (Tbl_User_Payment_Transactions payment in userTxns)
                        {
                            singleOrder.productId.Add(payment.Product_Id);
                            if (payment.Product_Id == 0 || payment.Product_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.productName.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault().Product_Name);
                            }
                            singleOrder.packageId.Add(payment.Package_Id);
                            if (payment.Package_Id == 0 || payment.Package_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.packageName.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault().Package_Name);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }
            return orders.OrderByDescending(x => x.paymentTransactionId).ToList();
        }

        public List<Def_Categories> getCategoriesBySuperCategory(int superCatId)
        {
            List<Def_Categories> categories = new List<Def_Categories>();
            try
            {
                categories = (from cat in context.Def_Categories
                              where cat.Super_Category_Id == superCatId && cat.Is_Active == true && cat.Is_Deleted == false
                              select cat).ToList();
            }
            catch (Exception ex)
            {

            }
            return categories;
        }

        public List<OrdersList> getOrders()
        {
            List<OrdersList> orders = new List<OrdersList>();
            try
            {
                orders = (from txn in context.Tbl_Payment_Transactions
                          join user in context.Tbl_User_Info
                          on txn.User_Id equals user.User_Id
                          join orderStatus in context.Def_Order_Current_Status
                          on txn.Order_Current_Status equals orderStatus.OrderCurrentStatusId
                          select new OrdersList
                          {
                              paymentTransactionId = txn.PaymentTransaction_Id,
                              PaymentMode = txn.Payment_Mode,
                              TxnAmount = txn.Txn_Amount,
                              TxnStatus = txn.Txn_Status,
                              orderCurrentStatus = orderStatus.OrderCurrentStatus,
                              TxnDate = txn.Txn_Date,
                              userId = user.User_Id,
                              userName = user.First_Name,
                              OrderCurrentStatusId = (int)txn.Order_Current_Status,
                              ShipmentId = txn.Shipment_Id,
                              CourierName = txn.Courier_Name,
                              PromoCode = txn.Promo_Code
                          }).ToList();

                foreach (OrdersList singleOrder in orders)
                {
                    List<Tbl_User_Payment_Transactions> userTxns = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == singleOrder.paymentTransactionId).ToList();
                    singleOrder.productId = new List<long?>();
                    singleOrder.productName = new List<string>();
                    singleOrder.packageId = new List<long?>();
                    singleOrder.packageName = new List<string>();
                    foreach (Tbl_User_Payment_Transactions payment in userTxns)
                    {
                        singleOrder.productId.Add(payment.Product_Id);
                        if (payment.Product_Id == 0 || payment.Product_Id == null)
                        {

                        }
                        else
                        {
                            singleOrder.productName.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault().Product_Name);
                        }
                        singleOrder.packageId.Add(payment.Package_Id);
                        if (payment.Package_Id == 0 || payment.Package_Id == null)
                        {

                        }
                        else
                        {
                            singleOrder.packageName.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault().Package_Name);
                        }
                    }
                }


            }
            catch (Exception ex)
            {

            }
            return orders.OrderByDescending(x => x.paymentTransactionId).ToList();
        }

        public PackageInfo GetPackageDetails(long packageId)
        {
            PackageInfo packageInfo = new PackageInfo();
            try
            {
                packageInfo = (from package in context.Tbl_Packages_Info
                               where package.Package_Id == packageId
                               select new PackageInfo
                               {
                                   packageDetails = package
                               }).FirstOrDefault();
                packageInfo.packageProducts = context.Tbl_Package_Products.Where(x => x.Package_Id == packageId).ToList();
                packageInfo.productInfo = new List<Tbl_Product_Info>();
                packageInfo.galleryInfo = new List<Tbl_Product_Gallery>();
                foreach (Tbl_Package_Products products in packageInfo.packageProducts)
                {
                    packageInfo.productInfo.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == products.Product_Id).FirstOrDefault());
                    packageInfo.galleryInfo.Add(context.Tbl_Product_Gallery.Where(x => x.Product_Id == products.Product_Id).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
            }
            return packageInfo;
        }

        public List<OrdersList> getPickupOrders()
        {
            List<OrdersList> orders = new List<OrdersList>();
            try
            {
                try
                {
                    orders = (from txn in context.Tbl_Payment_Transactions
                              join user in context.Tbl_User_Info
                              on txn.User_Id equals user.User_Id
                              join orderStatus in context.Def_Order_Current_Status
                              on txn.Order_Current_Status equals orderStatus.OrderCurrentStatusId
                              where txn.Authorized == true && txn.Pickup == true && (txn.Dispatched == false || txn.Dispatched == null) && (txn.Delivered == false || txn.Delivered == null)
                              && txn.Txn_Status == "SUCCESS" && txn.Order_Current_Status == 3
                              select new OrdersList
                              {
                                  paymentTransactionId = txn.PaymentTransaction_Id,
                                  PaymentMode = txn.Payment_Mode,
                                  TxnAmount = txn.Txn_Amount,
                                  TxnStatus = txn.Txn_Status,
                                  orderCurrentStatus = orderStatus.OrderCurrentStatus,
                                  TxnDate = txn.Txn_Date,
                                  userId = user.User_Id,
                                  userName = user.First_Name
                              }).ToList();

                    foreach (OrdersList singleOrder in orders)
                    {
                        List<Tbl_User_Payment_Transactions> userTxns = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == singleOrder.paymentTransactionId).ToList();
                        singleOrder.productId = new List<long?>();
                        singleOrder.productName = new List<string>();
                        singleOrder.packageId = new List<long?>();
                        singleOrder.packageName = new List<string>();
                        foreach (Tbl_User_Payment_Transactions payment in userTxns)
                        {
                            singleOrder.productId.Add(payment.Product_Id);
                            if (payment.Product_Id == 0 || payment.Product_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.productName.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault().Product_Name);
                            }
                            singleOrder.packageId.Add(payment.Package_Id);
                            if (payment.Package_Id == 0 || payment.Package_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.packageName.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault().Package_Name);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }
            return orders.OrderByDescending(x => x.paymentTransactionId).ToList();
        }

        public List<OrdersList> getDispatchedOrders()
        {
            List<OrdersList> orders = new List<OrdersList>();
            try
            {
                try
                {
                    orders = (from txn in context.Tbl_Payment_Transactions
                              join user in context.Tbl_User_Info
                              on txn.User_Id equals user.User_Id
                              join orderStatus in context.Def_Order_Current_Status
                              on txn.Order_Current_Status equals orderStatus.OrderCurrentStatusId
                              where txn.Authorized == true && txn.Pickup == true && txn.Dispatched == true && (txn.Delivered == false || txn.Delivered == null)
                              && txn.Txn_Status == "SUCCESS" && txn.Order_Current_Status == 4
                              select new OrdersList
                              {
                                  paymentTransactionId = txn.PaymentTransaction_Id,
                                  PaymentMode = txn.Payment_Mode,
                                  TxnAmount = txn.Txn_Amount,
                                  TxnStatus = txn.Txn_Status,
                                  orderCurrentStatus = orderStatus.OrderCurrentStatus,
                                  TxnDate = txn.Txn_Date,
                                  userId = user.User_Id,
                                  userName = user.First_Name
                              }).ToList();

                    foreach (OrdersList singleOrder in orders)
                    {
                        List<Tbl_User_Payment_Transactions> userTxns = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == singleOrder.paymentTransactionId).ToList();
                        singleOrder.productId = new List<long?>();
                        singleOrder.productName = new List<string>();
                        singleOrder.packageId = new List<long?>();
                        singleOrder.packageName = new List<string>();
                        foreach (Tbl_User_Payment_Transactions payment in userTxns)
                        {
                            singleOrder.productId.Add(payment.Product_Id);
                            if (payment.Product_Id == 0 || payment.Product_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.productName.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault().Product_Name);
                            }
                            singleOrder.packageId.Add(payment.Package_Id);
                            if (payment.Package_Id == 0 || payment.Package_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.packageName.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault().Package_Name);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }
            return orders.OrderByDescending(x => x.paymentTransactionId).ToList();
        }

        public List<OrdersList> getDeliveredOrders()
        {
            List<OrdersList> orders = new List<OrdersList>();
            try
            {
                try
                {
                    orders = (from txn in context.Tbl_Payment_Transactions
                              join user in context.Tbl_User_Info
                              on txn.User_Id equals user.User_Id
                              join orderStatus in context.Def_Order_Current_Status
                              on txn.Order_Current_Status equals orderStatus.OrderCurrentStatusId
                              where txn.Authorized == true && txn.Pickup == true && txn.Dispatched == true && txn.Delivered == true
                              && txn.Txn_Status == "SUCCESS" && txn.Order_Current_Status == 5
                              select new OrdersList
                              {
                                  paymentTransactionId = txn.PaymentTransaction_Id,
                                  PaymentMode = txn.Payment_Mode,
                                  TxnAmount = txn.Txn_Amount,
                                  TxnStatus = txn.Txn_Status,
                                  orderCurrentStatus = orderStatus.OrderCurrentStatus,
                                  TxnDate = txn.Txn_Date,
                                  userId = user.User_Id,
                                  userName = user.First_Name
                              }).ToList();

                    foreach (OrdersList singleOrder in orders)
                    {
                        List<Tbl_User_Payment_Transactions> userTxns = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == singleOrder.paymentTransactionId).ToList();
                        singleOrder.productId = new List<long?>();
                        singleOrder.productName = new List<string>();
                        singleOrder.packageId = new List<long?>();
                        singleOrder.packageName = new List<string>();
                        foreach (Tbl_User_Payment_Transactions payment in userTxns)
                        {
                            singleOrder.productId.Add(payment.Product_Id);
                            if (payment.Product_Id == 0 || payment.Product_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.productName.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault().Product_Name);
                            }
                            singleOrder.packageId.Add(payment.Package_Id);
                            if (payment.Package_Id == 0 || payment.Package_Id == null)
                            {

                            }
                            else
                            {
                                singleOrder.packageName.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault().Package_Name);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }
            return orders.OrderByDescending(x => x.paymentTransactionId).ToList();
        }

        public ProductInfo getProductDetails(long id)
        {
            ProductInfo product = new ProductInfo();
            try
            {
                product = (from pro in context.Tbl_Product_Info
                           join sub in context.Def_Sub_Categories
                           on pro.Sub_Category_Id equals sub.Sub_Category_Id
                           join cat in context.Def_Categories
                           on sub.Category_Id equals cat.Category_Id
                           join super in context.Def_Super_Categories
                           on cat.Super_Category_Id equals super.Super_Category_Id
                           join brand in context.Def_Brand_Info
                           on pro.Brand_Id equals brand.Brand_Id
                           where pro.Product_Id == id
                           select new ProductInfo
                           {
                               brandInfo = brand,
                               categoryInfo = cat,
                               productData = pro,
                               subCategoryInfo = sub,
                               superCategoryInfo = super
                           }).FirstOrDefault();

                product.productGallery = context.Tbl_Product_Gallery.Where(x => x.Product_Id == id).ToList();
                if (product.productData.Product_Sub_Type_Id != null)
                {
                    product.subProductTypeInfo = context.Tbl_Product_Sub_Types.Where(x => x.Product_Sub_Type_Id == product.productData.Product_Sub_Type_Id).FirstOrDefault();
                    product.productTypeInfo = context.Def_Product_Type.Where(x => x.Product_Type_Id == product.subProductTypeInfo.Product_Type_Id).FirstOrDefault();
                }
                if (product.productData.Generic_Id != null)
                {
                    product.genericName = context.Def_Generic_Names.Where(x => x.Generic_Id == product.productData.Generic_Id).FirstOrDefault().Generic_Name;
                }

                if (product.productData.Have_SubProducts == true)
                {
                    product.subProductsInfo = new List<SubProductInventory>();
                    List<Tbl_Sub_Product_Info> subProductInfo = context.Tbl_Sub_Product_Info.Where(x => x.Product_Id == product.productData.Product_Id && x.Is_Active == true && x.Is_Deleted == false).ToList();
                    foreach (Tbl_Sub_Product_Info sub in subProductInfo)
                    {
                        SubProductInventory inventory = new SubProductInventory();
                        inventory.Pack_Of_Products_Count = sub.Pack_Of_Products_Count;
                        inventory.Pack_Of_Products_Discount_Percentage = sub.Pack_Of_Products_Discount_Percentage;
                        inventory.Pack_Of_Products_Final_Cost = sub.Pack_Of_Products_Final_Cost;
                        product.subProductsInfo.Add(inventory);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return product;
        }

        public List<Tbl_Product_Sub_Types> getProductSubType(int typeId)
        {
            List<Tbl_Product_Sub_Types> subTypes = new List<Tbl_Product_Sub_Types>();
            try
            {
                subTypes = (from sub in context.Tbl_Product_Sub_Types
                            where sub.Product_Type_Id == typeId && sub.Is_Active == true && sub.Is_Deleted == false
                            select sub).ToList();
            }
            catch (Exception ex)
            {

            }
            return subTypes;
        }

        public List<Def_Product_Type> getProductTypes()
        {
            List<Def_Product_Type> productTypes = new List<Def_Product_Type>();
            try
            {
                productTypes = (from type in context.Def_Product_Type
                                where type.Is_Active == true && type.Is_Deleted == false
                                select type).ToList();
            }
            catch (Exception ex)
            {

            }
            return productTypes;
        }

        public List<Def_Product_Type> getAllProductTypes()
        {
            List<Def_Product_Type> productTypes = new List<Def_Product_Type>();
            try
            {
                productTypes = (from type in context.Def_Product_Type
                                select type).ToList();
            }
            catch (Exception ex)
            {

            }
            return productTypes;
        }

        public string getProudtcGallery(long productId)
        {
            string imagePath = "";
            try
            {
                imagePath = context.Tbl_Product_Gallery.Where(x => x.Product_Id == productId).FirstOrDefault().Img_Url;
            }
            catch (Exception ex)
            {

            }
            return imagePath;
        }

        public List<Def_Sub_Categories> getSubCategoriesByCategory(int catId)
        {
            List<Def_Sub_Categories> subCategories = new List<Def_Sub_Categories>();
            try
            {
                subCategories = (from sub in context.Def_Sub_Categories
                                 where sub.Category_Id == catId && sub.Is_Active == true && sub.Is_Deleted == false
                                 select sub).ToList();
            }
            catch (Exception ex)
            {

            }
            return subCategories;
        }

        public List<Def_Super_Categories> getSuperCategories()
        {
            List<Def_Super_Categories> superCategories = new List<Def_Super_Categories>();
            try
            {
                superCategories = (from super in context.Def_Super_Categories
                                   where super.Is_Active == true && super.Is_Deleted == false
                                   select super).ToList();
            }
            catch (Exception ex)
            {

            }
            return superCategories;
        }

        public List<Def_Super_Categories> getAllSuperCategories()
        {
            List<Def_Super_Categories> superCategories = new List<Def_Super_Categories>();
            try
            {
                superCategories = (from super in context.Def_Super_Categories
                                   select super).ToList();
            }
            catch (Exception ex)
            {

            }
            return superCategories;
        }

        public List<Def_Brand_Info> SearchAllBrandList(string sord)
        {
            List<Def_Brand_Info> data = new List<Def_Brand_Info>();
            try
            {
                data = (from brands in context.Def_Brand_Info
                        where brands.Brand_Name.Contains(sord)
                        && brands.Is_Active == true && brands.Is_Deleted == false
                        select brands).ToList();
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<Def_Generic_Names> SearchAllGenericList(string sord)
        {
            List<Def_Generic_Names> data = new List<Def_Generic_Names>();
            try
            {
                data = (from generic in context.Def_Generic_Names
                        where generic.Generic_Name.Contains(sord)
                        && generic.Is_Active == true && generic.Is_Deleted == false
                        select generic).ToList();
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<string> SearchAllProductList(string sord)
        {
            List<string> data = new List<string>();
            try
            {
                data = (from product in context.Tbl_Product_Info
                        where product.Product_Name.Contains(sord)
                        && product.Is_Active == true && product.Is_Deleted == false
                        select product.Product_Name).ToList();
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<Tbl_Product_Info> SearchAutoCompleteProductList(string sord)
        {
            List<Tbl_Product_Info> products = new List<Tbl_Product_Info>();
            try
            {
                products = (from product in context.Tbl_Product_Info
                            where product.Product_Name.Contains(sord)
                            && product.Is_Active == true && product.Is_Deleted == false
                            select product).ToList();
            }
            catch (Exception ex)
            {

            }
            return products;
        }

        public Tbl_User_Info userLogin(string userName, string password)
        {
            Tbl_User_Info userData = new Tbl_User_Info();
            try
            {
                int userRole = (int)Shared.UserRoles.User;
                userData = (from user in context.Tbl_User_Info
                            where user.Role_Id != userRole
                            && user.Email_ID == userName && user.Password == password
                            select user
                            ).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return userData;
        }

        public int CreateShipment(string ids, string courierName)
        {
            int i = 0;
            try
            {
                foreach (string id in ids.Split(','))
                {
                    long orderId = Convert.ToInt64(id);

                    Tbl_Payment_Transactions paymentOrder = context.Tbl_Payment_Transactions.Where(x => x.PaymentTransaction_Id == orderId).FirstOrDefault();
                    if (paymentOrder != null)
                    {
                        paymentOrder.Pickup = true;
                        paymentOrder.Order_Current_Status = 3;
                        paymentOrder.Shipment_Date = DateTime.Now;
                        paymentOrder.Courier_Name = courierName;
                    }
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int MoveToDispatchedOrders(string ids, string pickupId, string shipmentId)
        {
            int i = 0;
            try
            {
                foreach (string id in ids.Split(','))
                {
                    long orderId = Convert.ToInt64(id);

                    Tbl_Payment_Transactions paymentOrder = context.Tbl_Payment_Transactions.Where(x => x.PaymentTransaction_Id == orderId).FirstOrDefault();
                    if (paymentOrder != null)
                    {
                        paymentOrder.Dispatched = true;
                        paymentOrder.Order_Current_Status = 4;
                        paymentOrder.Pickup_Date = DateTime.Now;
                        paymentOrder.Shipment_Id = shipmentId;
                        paymentOrder.Pickup_Id = pickupId;
                    }
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int MoveToDeliveredOrders(string ids, string courierBoyName, string courierBoyNumber)
        {
            int i = 0;
            try
            {
                foreach (string id in ids.Split(','))
                {
                    long orderId = Convert.ToInt64(id);

                    Tbl_Payment_Transactions paymentOrder = context.Tbl_Payment_Transactions.Where(x => x.PaymentTransaction_Id == orderId).FirstOrDefault();
                    if (paymentOrder != null)
                    {
                        paymentOrder.Delivered = true;
                        paymentOrder.Order_Current_Status = 5;
                        paymentOrder.Dispatched_Date = DateTime.Now;
                        paymentOrder.Courier_Person_Contact = courierBoyNumber;
                        paymentOrder.Courier_Person_Name = courierBoyName;
                    }
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public OrderDetails getOrderInfo(long id)
        {
            OrderDetails orderInfo = new OrderDetails();
            try
            {
                orderInfo = (from order in context.Tbl_Payment_Transactions
                             join user in context.Tbl_User_Info
                             on order.User_Id equals user.User_Id
                             where order.PaymentTransaction_Id == id
                             select new OrderDetails
                             {
                                 orderInfo = order,
                                 userInfo = user,
                                 orderCurrentStatus = context.Def_Order_Current_Status.Where(x => x.OrderCurrentStatusId == order.Order_Current_Status).FirstOrDefault().OrderCurrentStatus
                             }).FirstOrDefault();

                orderInfo.paymentInfo = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == id).ToList();
                orderInfo.productsInfo = new List<Tbl_Product_Info>();
                orderInfo.packagesInfo = new List<Tbl_Packages_Info>();
                orderInfo.userAddresses = new List<Tbl_User_Addresses>();
                orderInfo.prescriptionList = new List<Tbl_Presecription_Info>();
                foreach (Tbl_User_Payment_Transactions payment in orderInfo.paymentInfo)
                {
                    if (payment.Product_Id == 0 || payment.Product_Id == null)
                    {

                    }
                    else
                    {
                        orderInfo.productsInfo.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault());

                    }
                    if (payment.Package_Id == 0 || payment.Package_Id == null)
                    {

                    }
                    else
                    {
                        orderInfo.packagesInfo.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault());
                    }

                    orderInfo.userAddresses.Add(context.Tbl_User_Addresses.Where(x => x.AddressId == payment.Shipping_Address_Id).FirstOrDefault());
                    orderInfo.userAddresses.Add(context.Tbl_User_Addresses.Where(x => x.AddressId == payment.Billing_Address_Id).FirstOrDefault());

                    orderInfo.prescriptionList = context.Tbl_Presecription_Info.Where(x => x.Payment_Transaction_Id == payment.Payment_Transaction_Id && x.Status == true).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return orderInfo;
        }

        public List<Tbl_User_Info> getAllUsers()
        {
            List<Tbl_User_Info> usersList = new List<Tbl_User_Info>();
            try
            {
                usersList = context.Tbl_User_Info.Where(x => x.Role_Id == 6).ToList();
            }
            catch (Exception ex)
            {

            }
            return usersList;
        }

        public List<OrdersList> getUserOrders(long id)
        {
            List<OrdersList> orders = new List<OrdersList>();
            try
            {
                orders = (from txn in context.Tbl_Payment_Transactions
                          join user in context.Tbl_User_Info
                          on txn.User_Id equals user.User_Id
                          join orderStatus in context.Def_Order_Current_Status
                          on txn.Order_Current_Status equals orderStatus.OrderCurrentStatusId
                          where txn.User_Id == id
                          select new OrdersList
                          {
                              paymentTransactionId = txn.PaymentTransaction_Id,
                              PaymentMode = txn.Payment_Mode,
                              TxnAmount = txn.Txn_Amount,
                              TxnStatus = txn.Txn_Status,
                              orderCurrentStatus = orderStatus.OrderCurrentStatus,
                              TxnDate = txn.Txn_Date,
                              userId = user.User_Id,
                              userName = user.First_Name
                          }).ToList();

                foreach (OrdersList singleOrder in orders)
                {
                    List<Tbl_User_Payment_Transactions> userTxns = context.Tbl_User_Payment_Transactions.Where(x => x.Payment_Transaction_Id == singleOrder.paymentTransactionId).ToList();
                    singleOrder.productId = new List<long?>();
                    singleOrder.productName = new List<string>();
                    singleOrder.packageId = new List<long?>();
                    singleOrder.packageName = new List<string>();
                    foreach (Tbl_User_Payment_Transactions payment in userTxns)
                    {
                        singleOrder.productId.Add(payment.Product_Id);
                        if (payment.Product_Id == 0 || payment.Product_Id == null)
                        {

                        }
                        else
                        {
                            singleOrder.productName.Add(context.Tbl_Product_Info.Where(x => x.Product_Id == payment.Product_Id).FirstOrDefault().Product_Name);
                        }
                        singleOrder.packageId.Add(payment.Package_Id);
                        if (payment.Package_Id == 0 || payment.Package_Id == null)
                        {

                        }
                        else
                        {
                            singleOrder.packageName.Add(context.Tbl_Packages_Info.Where(x => x.Package_Id == payment.Package_Id).FirstOrDefault().Package_Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return orders.OrderByDescending(x => x.paymentTransactionId).ToList();
        }

        public int CancelOrder(long orderId)
        {
            int i = 0;
            try
            {
                Tbl_Payment_Transactions paymentOrder = context.Tbl_Payment_Transactions.Where(x => x.PaymentTransaction_Id == orderId).FirstOrDefault();
                if (paymentOrder != null)
                {
                    paymentOrder.Order_Current_Status = 7;
                    paymentOrder.Txn_Status = "Cancelled";
                    paymentOrder.Txn_Message = "Cancelled By Admin";
                }
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int AddSuperCategory(Def_Super_Categories superCatInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                if (context.Def_Super_Categories.Where(x => x.Super_Category_Name == superCatInfo.Super_Category_Name && x.Is_Active == true && x.Is_Deleted == false).Count() > 0)
                {
                    i = -1;
                }
                else
                {
                    Def_Super_Categories super = new Def_Super_Categories()
                    {
                        Super_Category_Name = superCatInfo.Super_Category_Name,
                        Created_By = userData.User_Id,
                        Updated_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now,
                        Is_Active = true,
                        Is_Deleted = false
                    };
                    context.Def_Super_Categories.Add(super);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int AddCategory(Def_Categories catInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                if (context.Def_Categories.Where(x => x.Category_Name == catInfo.Category_Name && x.Super_Category_Id == catInfo.Super_Category_Id && x.Is_Active == true && x.Is_Deleted == false).Count() > 0)
                {
                    i = -1;
                }
                else
                {
                    Def_Categories cat = new Def_Categories()
                    {
                        Super_Category_Id = catInfo.Super_Category_Id,
                        Category_Name = catInfo.Category_Name,
                        Created_By = userData.User_Id,
                        Updated_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now,
                        Is_Active = true,
                        Is_Deleted = false
                    };
                    context.Def_Categories.Add(cat);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<CategoryList> getAllCategories()
        {
            List<CategoryList> catList = new List<CategoryList>();
            try
            {
                catList = (from cat in context.Def_Categories
                           join super in context.Def_Super_Categories
                           on cat.Super_Category_Id equals super.Super_Category_Id
                           where super.Is_Active == true && super.Is_Deleted == false
                           select new CategoryList
                           {
                               CategoryId = cat.Category_Id,
                               CategoryName = cat.Category_Name,
                               SuperCategoryId = super.Super_Category_Id,
                               SuperCategoryName = super.Super_Category_Name,
                               Is_Active = cat.Is_Active,
                               Is_Deleted = cat.Is_Deleted
                           }).ToList();
            }
            catch (Exception ex)
            {

            }
            return catList;
        }

        public List<SubCategoriesList> getAllSubCategories()
        {
            List<SubCategoriesList> subCategories = new List<SubCategoriesList>();
            try
            {
                subCategories = (from sub in context.Def_Sub_Categories
                                 join cat in context.Def_Categories
                                 on sub.Category_Id equals cat.Category_Id
                                 join super in context.Def_Super_Categories
                                 on cat.Super_Category_Id equals super.Super_Category_Id
                                 where cat.Is_Active == true && cat.Is_Deleted == false
                                 select new SubCategoriesList
                                 {
                                     CategoryId = cat.Category_Id,
                                     CategoryName = cat.Category_Name,
                                     SubCategoryId = sub.Sub_Category_Id,
                                     SubCategoryName = sub.Sub_Category_Name,
                                     SuperCategoryId = super.Super_Category_Id,
                                     SuperCategoryName = super.Super_Category_Name,
                                     Is_Active = sub.Is_Active,
                                     Is_Deleted = sub.Is_Deleted
                                 }).ToList();
            }
            catch (Exception ex)
            {

            }
            return subCategories;
        }

        public int AddSubCategory(Def_Sub_Categories subCatInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                if (context.Def_Sub_Categories.Where(x => x.Sub_Category_Name == subCatInfo.Sub_Category_Name && x.Category_Id == subCatInfo.Category_Id && x.Is_Active == true && x.Is_Deleted == false).Count() > 0)
                {
                    i = -1;
                }
                else
                {
                    Def_Sub_Categories sub = new Def_Sub_Categories()
                    {
                        Sub_Category_Name = subCatInfo.Sub_Category_Name,
                        Category_Id = subCatInfo.Category_Id,
                        Created_By = userData.User_Id,
                        Updated_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now,
                        Is_Active = true,
                        Is_Deleted = false
                    };
                    context.Def_Sub_Categories.Add(sub);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int AddProductType(Def_Product_Type typeInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                if (context.Def_Product_Type.Where(x => x.Product_Type == typeInfo.Product_Type && x.Is_Active == true && x.Is_Deleted == false).Count() > 0)
                {
                    i = -1;
                }
                else
                {
                    Def_Product_Type type = new Def_Product_Type()
                    {
                        Product_Type = typeInfo.Product_Type,
                        Created_By = userData.User_Id,
                        Updated_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now,
                        Is_Active = true,
                        Is_Deleted = false
                    };
                    context.Def_Product_Type.Add(type);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<ProductSubTypeList> getAllProductSubTypes()
        {
            List<ProductSubTypeList> subTypeList = new List<ProductSubTypeList>();
            try
            {
                subTypeList = (from subType in context.Tbl_Product_Sub_Types
                               join type in context.Def_Product_Type
                               on subType.Product_Type_Id equals type.Product_Type_Id
                               where type.Is_Active == true && type.Is_Deleted == false
                               select new ProductSubTypeList
                               {
                                   productSubType = subType.Product_Sub_Type,
                                   productSubTypeId = subType.Product_Sub_Type_Id,
                                   productType = type.Product_Type,
                                   productTypeId = type.Product_Type_Id,
                                   Is_Active = subType.Is_Active,
                                   Is_Deleted = subType.Is_Deleted
                               }).ToList();
            }
            catch (Exception ex)
            {

            }
            return subTypeList;
        }

        public int AddProductSubType(Tbl_Product_Sub_Types subTypeInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                if (context.Tbl_Product_Sub_Types.Where(x => x.Product_Sub_Type == subTypeInfo.Product_Sub_Type && x.Product_Type_Id == subTypeInfo.Product_Type_Id && x.Is_Active == true && x.Is_Deleted == false).Count() > 0)
                {
                    i = -1;
                }
                else
                {
                    Tbl_Product_Sub_Types subType = new Tbl_Product_Sub_Types()
                    {
                        Created_By = userData.User_Id,
                        Updated_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Updated_Date = DateTime.Now,
                        Is_Active = true,
                        Is_Deleted = false,
                        Product_Sub_Type = subTypeInfo.Product_Sub_Type,
                        Product_Type_Id = subTypeInfo.Product_Type_Id
                    };
                    context.Tbl_Product_Sub_Types.Add(subType);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int EditSuperCategoryInfo(Def_Super_Categories supCatInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Super_Categories superCateInfo = context.Def_Super_Categories.Where(x => x.Super_Category_Id == supCatInfo.Super_Category_Id).FirstOrDefault();
                if (superCateInfo != null)
                {
                    superCateInfo.Super_Category_Name = supCatInfo.Super_Category_Name;
                    superCateInfo.Updated_By = userData.User_Id;
                    superCateInfo.Updated_Date = DateTime.Now;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public Def_Categories getCategoryByCategoryId(int id)
        {
            Def_Categories cat = new Def_Categories();
            try
            {
                cat = context.Def_Categories.Where(x => x.Category_Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return cat;
        }

        public int EditCategoryInfo(Def_Categories catInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Categories categoriesInfo = context.Def_Categories.Where(x => x.Category_Id == catInfo.Category_Id).FirstOrDefault();
                if (categoriesInfo != null)
                {
                    categoriesInfo.Category_Name = catInfo.Category_Name;
                    categoriesInfo.Super_Category_Id = catInfo.Super_Category_Id;
                    categoriesInfo.Updated_By = userData.User_Id;
                    categoriesInfo.Updated_Date = DateTime.Now;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public SubCategoriesList getSubCategoryBySubCategoryId(long id)
        {
            SubCategoriesList subCatInfo = new SubCategoriesList();
            try
            {
                subCatInfo = (from sub in context.Def_Sub_Categories
                              join cat in context.Def_Categories
                              on sub.Category_Id equals cat.Category_Id
                              join super in context.Def_Super_Categories
                              on cat.Super_Category_Id equals super.Super_Category_Id
                              where sub.Sub_Category_Id == id
                              select new SubCategoriesList
                              {
                                  CategoryId = cat.Category_Id,
                                  CategoryName = cat.Category_Name,
                                  SubCategoryId = sub.Sub_Category_Id,
                                  SubCategoryName = sub.Sub_Category_Name,
                                  SuperCategoryId = super.Super_Category_Id,
                                  SuperCategoryName = super.Super_Category_Name
                              }).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return subCatInfo;
        }

        public int EditSubCategoryInfo(Def_Sub_Categories subCatInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Sub_Categories subCatList = context.Def_Sub_Categories.Where(x => x.Sub_Category_Id == subCatInfo.Sub_Category_Id).FirstOrDefault();
                if (subCatList != null)
                {
                    subCatList.Sub_Category_Name = subCatInfo.Sub_Category_Name;
                    subCatList.Updated_By = userData.User_Id;
                    subCatList.Updated_Date = DateTime.Now;
                    subCatList.Category_Id = subCatInfo.Category_Id;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int EditProductTypeInfo(Def_Product_Type productTypeInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Product_Type typeInfo = context.Def_Product_Type.Where(x => x.Product_Type_Id == productTypeInfo.Product_Type_Id).FirstOrDefault();
                if (typeInfo != null)
                {
                    typeInfo.Product_Type = productTypeInfo.Product_Type;
                    typeInfo.Updated_By = userData.User_Id;
                    typeInfo.Updated_Date = DateTime.Now;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public Tbl_Product_Sub_Types getSubTypesBySubTypeId(int id)
        {
            Tbl_Product_Sub_Types sub = new Tbl_Product_Sub_Types();
            try
            {
                sub = context.Tbl_Product_Sub_Types.Where(x => x.Product_Sub_Type_Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return sub;
        }

        public int EditProductSubTypeInfo(Tbl_Product_Sub_Types subTypeInfo, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Tbl_Product_Sub_Types subTypedata = context.Tbl_Product_Sub_Types.Where(x => x.Product_Sub_Type_Id == subTypeInfo.Product_Sub_Type_Id).FirstOrDefault();
                if (subTypedata != null)
                {
                    subTypedata.Product_Sub_Type = subTypeInfo.Product_Sub_Type;
                    subTypedata.Product_Type_Id = subTypeInfo.Product_Type_Id;
                    subTypedata.Updated_By = userData.User_Id;
                    subTypedata.Updated_Date = DateTime.Now;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int getSuperCategoryProductsCount(int superCatId)
        {
            int i = 0;
            try
            {
                i = (from super in context.Def_Super_Categories
                     join cat in context.Def_Categories
                     on super.Super_Category_Id equals cat.Super_Category_Id
                     join sub in context.Def_Sub_Categories
                     on cat.Category_Id equals sub.Category_Id
                     join pro in context.Tbl_Product_Info
                     on sub.Sub_Category_Id equals pro.Sub_Category_Id
                     where super.Super_Category_Id == superCatId
                     select pro).Count();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int ChangeSuperCategoryStatus(int superCatId, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Super_Categories superCatInfo = context.Def_Super_Categories.Where(x => x.Super_Category_Id == superCatId).FirstOrDefault();
                if (superCatInfo != null)
                {
                    if (superCatInfo.Is_Active == true && superCatInfo.Is_Deleted == false)
                    {
                        superCatInfo.Is_Active = false;
                        superCatInfo.Is_Deleted = true;
                        superCatInfo.Updated_By = userData.User_Id;
                        superCatInfo.Updated_Date = DateTime.Now;
                        List<Def_Categories> categoriesOfSuperCategory = context.Def_Categories.Where(x => x.Super_Category_Id == superCatInfo.Super_Category_Id).ToList();
                        foreach (Def_Categories catInfo in categoriesOfSuperCategory)
                        {
                            catInfo.Is_Active = false;
                            catInfo.Is_Deleted = true;
                            catInfo.Updated_By = userData.User_Id;
                            catInfo.Updated_Date = DateTime.Now;
                            List<Def_Sub_Categories> subCategoriesOfCategory = context.Def_Sub_Categories.Where(x => x.Category_Id == catInfo.Category_Id).ToList();
                            foreach (Def_Sub_Categories subCat in subCategoriesOfCategory)
                            {
                                subCat.Is_Active = false;
                                subCat.Is_Deleted = true;
                                subCat.Updated_By = userData.User_Id;
                                subCat.Updated_Date = DateTime.Now;
                                List<Tbl_Product_Info> productsOfSubCategory = context.Tbl_Product_Info.Where(x => x.Sub_Category_Id == subCat.Sub_Category_Id).ToList();

                                foreach (Tbl_Product_Info product in productsOfSubCategory)
                                {
                                    product.Is_Active = false;
                                    product.Is_Deleted = true;
                                    product.Updated_By = userData.User_Id;
                                    product.Updated_Date = DateTime.Now;
                                }
                            }
                        }
                    }
                    else
                    {
                        superCatInfo.Is_Active = true;
                        superCatInfo.Is_Deleted = false;
                        superCatInfo.Updated_By = userData.User_Id;
                        superCatInfo.Updated_Date = DateTime.Now;
                        List<Def_Categories> categoriesOfSuperCategory = context.Def_Categories.Where(x => x.Super_Category_Id == superCatInfo.Super_Category_Id).ToList();
                        foreach (Def_Categories catInfo in categoriesOfSuperCategory)
                        {
                            catInfo.Is_Active = true;
                            catInfo.Is_Deleted = false;
                            catInfo.Updated_By = userData.User_Id;
                            catInfo.Updated_Date = DateTime.Now;
                            List<Def_Sub_Categories> subCategoriesOfCategory = context.Def_Sub_Categories.Where(x => x.Category_Id == catInfo.Category_Id).ToList();
                            foreach (Def_Sub_Categories subCat in subCategoriesOfCategory)
                            {
                                subCat.Is_Active = true;
                                subCat.Is_Deleted = false;
                                subCat.Updated_By = userData.User_Id;
                                subCat.Updated_Date = DateTime.Now;
                                List<Tbl_Product_Info> productsOfSubCategory = context.Tbl_Product_Info.Where(x => x.Sub_Category_Id == subCat.Sub_Category_Id).ToList();

                                foreach (Tbl_Product_Info product in productsOfSubCategory)
                                {
                                    product.Is_Active = true;
                                    product.Is_Deleted = false;
                                    product.Updated_By = userData.User_Id;
                                    product.Updated_Date = DateTime.Now;
                                }
                            }
                        }
                    }
                }
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int getCategoryProductsCount(int catId)
        {
            int i = 0;
            try
            {
                i = (from cat in context.Def_Categories
                     join sub in context.Def_Sub_Categories
                     on cat.Category_Id equals sub.Category_Id
                     join pro in context.Tbl_Product_Info
                     on sub.Sub_Category_Id equals pro.Sub_Category_Id
                     where cat.Category_Id == catId
                     select pro).Count();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int ChangeCategoryStatus(int catId, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Categories categoryInfo = context.Def_Categories.Where(x => x.Category_Id == catId).FirstOrDefault();
                if (categoryInfo != null)
                {
                    if (categoryInfo.Is_Deleted == false && categoryInfo.Is_Active == true)
                    {
                        categoryInfo.Is_Active = false;
                        categoryInfo.Is_Deleted = true;
                        categoryInfo.Updated_By = userData.User_Id;
                        categoryInfo.Updated_Date = DateTime.Now;
                        List<Def_Sub_Categories> subCategoriesOfCategory = context.Def_Sub_Categories.Where(x => x.Category_Id == categoryInfo.Category_Id).ToList();
                        foreach (Def_Sub_Categories subCat in subCategoriesOfCategory)
                        {
                            subCat.Is_Active = false;
                            subCat.Is_Deleted = true;
                            subCat.Updated_By = userData.User_Id;
                            subCat.Updated_Date = DateTime.Now;
                            List<Tbl_Product_Info> productsOfSubCategory = context.Tbl_Product_Info.Where(x => x.Sub_Category_Id == subCat.Sub_Category_Id).ToList();

                            foreach (Tbl_Product_Info product in productsOfSubCategory)
                            {
                                product.Is_Active = false;
                                product.Is_Deleted = true;
                                product.Updated_By = userData.User_Id;
                                product.Updated_Date = DateTime.Now;
                            }
                        }
                    }
                    else
                    {
                        categoryInfo.Is_Active = true;
                        categoryInfo.Is_Deleted = false;
                        categoryInfo.Updated_By = userData.User_Id;
                        categoryInfo.Updated_Date = DateTime.Now;
                        List<Def_Sub_Categories> subCategoriesOfCategory = context.Def_Sub_Categories.Where(x => x.Category_Id == categoryInfo.Category_Id).ToList();
                        foreach (Def_Sub_Categories subCat in subCategoriesOfCategory)
                        {
                            subCat.Is_Active = true;
                            subCat.Is_Deleted = false;
                            subCat.Updated_By = userData.User_Id;
                            subCat.Updated_Date = DateTime.Now;
                            List<Tbl_Product_Info> productsOfSubCategory = context.Tbl_Product_Info.Where(x => x.Sub_Category_Id == subCat.Sub_Category_Id).ToList();

                            foreach (Tbl_Product_Info product in productsOfSubCategory)
                            {
                                product.Is_Active = true;
                                product.Is_Deleted = false;
                                product.Updated_By = userData.User_Id;
                                product.Updated_Date = DateTime.Now;
                            }
                        }
                    }
                }

                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int getSubCategoryProductsCount(int subCatId)
        {
            int i = 0;
            try
            {
                i = (from sub in context.Def_Sub_Categories
                     join pro in context.Tbl_Product_Info
                     on sub.Sub_Category_Id equals pro.Sub_Category_Id
                     where sub.Sub_Category_Id == subCatId
                     select pro).Count();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int ChangeSubCategoryStatus(int subCatId, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Sub_Categories subCat = context.Def_Sub_Categories.Where(x => x.Sub_Category_Id == subCatId).FirstOrDefault();
                if (subCat != null)
                {
                    if (subCat.Is_Active == true && subCat.Is_Deleted == false)
                    {
                        subCat.Is_Active = false;
                        subCat.Is_Deleted = true;
                        subCat.Updated_By = userData.User_Id;
                        subCat.Updated_Date = DateTime.Now;
                        List<Tbl_Product_Info> productsOfSubCategory = context.Tbl_Product_Info.Where(x => x.Sub_Category_Id == subCat.Sub_Category_Id).ToList();

                        foreach (Tbl_Product_Info product in productsOfSubCategory)
                        {
                            product.Is_Active = false;
                            product.Is_Deleted = true;
                            product.Updated_By = userData.User_Id;
                            product.Updated_Date = DateTime.Now;
                        }
                    }
                    else
                    {
                        subCat.Is_Active = true;
                        subCat.Is_Deleted = false;
                        subCat.Updated_By = userData.User_Id;
                        subCat.Updated_Date = DateTime.Now;
                        List<Tbl_Product_Info> productsOfSubCategory = context.Tbl_Product_Info.Where(x => x.Sub_Category_Id == subCat.Sub_Category_Id).ToList();

                        foreach (Tbl_Product_Info product in productsOfSubCategory)
                        {
                            product.Is_Active = true;
                            product.Is_Deleted = false;
                            product.Updated_By = userData.User_Id;
                            product.Updated_Date = DateTime.Now;
                        }
                    }
                }
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int ChangeProductTypeStatus(int typeId, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Def_Product_Type typeInfo = context.Def_Product_Type.Where(x => x.Product_Type_Id == typeId).FirstOrDefault();
                if (typeInfo != null)
                {
                    if (typeInfo.Is_Active == true && typeInfo.Is_Deleted == false)
                    {
                        typeInfo.Is_Active = false;
                        typeInfo.Is_Deleted = true;
                        typeInfo.Updated_By = userData.User_Id;
                        typeInfo.Updated_Date = DateTime.Now;

                        List<Tbl_Product_Sub_Types> subTypesInfo = context.Tbl_Product_Sub_Types.Where(x => x.Product_Type_Id == typeId).ToList();
                        foreach (Tbl_Product_Sub_Types subInfo in subTypesInfo)
                        {
                            subInfo.Is_Active = false;
                            subInfo.Is_Deleted = true;
                            subInfo.Updated_By = userData.User_Id;
                            subInfo.Updated_Date = DateTime.Now;

                            List<Tbl_Product_Info> productsData = context.Tbl_Product_Info.Where(x => x.Product_Sub_Type_Id == subInfo.Product_Sub_Type_Id).ToList();
                            foreach (Tbl_Product_Info pro in productsData)
                            {
                                pro.Is_Active = false;
                                pro.Is_Deleted = true;
                                pro.Updated_By = userData.User_Id;
                                pro.Updated_Date = DateTime.Now;
                            }
                        }
                    }
                    else
                    {
                        typeInfo.Is_Active = true;
                        typeInfo.Is_Deleted = false;
                        typeInfo.Updated_By = userData.User_Id;
                        typeInfo.Updated_Date = DateTime.Now;

                        List<Tbl_Product_Sub_Types> subTypesInfo = context.Tbl_Product_Sub_Types.Where(x => x.Product_Type_Id == typeId).ToList();
                        foreach (Tbl_Product_Sub_Types subInfo in subTypesInfo)
                        {
                            subInfo.Is_Active = true;
                            subInfo.Is_Deleted = false;
                            subInfo.Updated_By = userData.User_Id;
                            subInfo.Updated_Date = DateTime.Now;

                            List<Tbl_Product_Info> productsData = context.Tbl_Product_Info.Where(x => x.Product_Sub_Type_Id == subInfo.Product_Sub_Type_Id).ToList();
                            foreach (Tbl_Product_Info pro in productsData)
                            {
                                pro.Is_Active = true;
                                pro.Is_Deleted = false;
                                pro.Updated_By = userData.User_Id;
                                pro.Updated_Date = DateTime.Now;
                            }
                        }
                    }
                }
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int getProductTypeProductsCount(int typeId)
        {
            int i = 0;
            try
            {
                i = (from type in context.Def_Product_Type
                     join subType in context.Tbl_Product_Sub_Types
                     on type.Product_Type_Id equals subType.Product_Type_Id
                     join pro in context.Tbl_Product_Info
                     on subType.Product_Sub_Type_Id equals pro.Product_Sub_Type_Id
                     where type.Product_Type_Id == typeId
                     select pro).Count();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int getProductSubTypeProductsCount(int subtypeId)
        {
            int i = 0;
            try
            {
                i = (from subType in context.Tbl_Product_Sub_Types
                     join pro in context.Tbl_Product_Info
                     on subType.Product_Sub_Type_Id equals pro.Product_Sub_Type_Id
                     where subType.Product_Sub_Type_Id == subtypeId
                     select pro).Count();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int ChangeProductSubTypeStatus(int subtypeId, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Tbl_Product_Sub_Types subType = context.Tbl_Product_Sub_Types.Where(x => x.Product_Sub_Type_Id == subtypeId).FirstOrDefault();
                if (subType != null)
                {
                    if (subType.Is_Active == true && subType.Is_Deleted == false)
                    {
                        subType.Is_Active = false;
                        subType.Is_Deleted = true;
                        subType.Updated_By = userData.User_Id;
                        subType.Updated_Date = DateTime.Now;
                        List<Tbl_Product_Info> productsOfSubType = context.Tbl_Product_Info.Where(x => x.Product_Sub_Type_Id == subType.Product_Sub_Type_Id).ToList();

                        foreach (Tbl_Product_Info product in productsOfSubType)
                        {
                            product.Is_Active = false;
                            product.Is_Deleted = true;
                            product.Updated_By = userData.User_Id;
                            product.Updated_Date = DateTime.Now;
                        }
                    }
                    else
                    {
                        subType.Is_Active = true;
                        subType.Is_Deleted = false;
                        subType.Updated_By = userData.User_Id;
                        subType.Updated_Date = DateTime.Now;
                        List<Tbl_Product_Info> productsOfSubType = context.Tbl_Product_Info.Where(x => x.Product_Sub_Type_Id == subType.Product_Sub_Type_Id).ToList();

                        foreach (Tbl_Product_Info product in productsOfSubType)
                        {
                            product.Is_Active = true;
                            product.Is_Deleted = false;
                            product.Updated_By = userData.User_Id;
                            product.Updated_Date = DateTime.Now;
                        }
                    }
                }
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<Def_Banner_Related_Info> getBannerRelatedInfo()
        {
            List<Def_Banner_Related_Info> BannerInfo = new List<Def_Banner_Related_Info>();
            try
            {
                BannerInfo = context.Def_Banner_Related_Info.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();
            }
            catch (Exception ex)
            {

            }
            return BannerInfo;
        }

        public List<Def_Brand_Info> getAllBrands()
        {
            List<Def_Brand_Info> brands = new List<Def_Brand_Info>();
            try
            {
                brands = context.Def_Brand_Info.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();
            }
            catch (Exception ex)
            {

            }
            return brands;
        }

        public int AddBannerInfo(Tbl_Banner_Gallery_Info bannerGallery, List<string> imagePaths, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                foreach (string image in imagePaths)
                {
                    Tbl_Banner_Gallery_Info gallery = new Tbl_Banner_Gallery_Info()
                    {
                        Banner_Image_Path = image,
                        Banner_Related_Id = bannerGallery.Banner_Related_Id,
                        Banner_Related_Value = bannerGallery.Banner_Related_Value,
                        Created_By = userData.User_Id,
                        Created_Date = DateTime.Now,
                        Is_Active = true,
                        Is_Deleted = false,
                        Is_Enabled = bannerGallery.Is_Enabled,
                        Updated_By = userData.User_Id,
                        Updated_Date = DateTime.Now
                    };

                    context.Tbl_Banner_Gallery_Info.Add(gallery);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<BannersInfo> getBannersInfo()
        {
            List<BannersInfo> banners = new List<BannersInfo>();
            try
            {
                banners = (from gallery in context.Tbl_Banner_Gallery_Info
                           join related in context.Def_Banner_Related_Info
                           on gallery.Banner_Related_Id equals related.Banner_Related_Id
                           where gallery.Is_Active == true && gallery.Is_Deleted == false
                           select new BannersInfo
                           {
                               BannerId = gallery.Banner_Id,
                               Banner_Related_To = gallery.Banner_Related_Id,
                               Banner_Related_Value = gallery.Banner_Related_Value,
                               Is_Enabled = gallery.Is_Enabled
                           }).ToList();

                foreach (BannersInfo bann in banners)
                {
                    bann.Banner_Images = new List<string>();
                    bann.Banner_Images.AddRange(context.Tbl_Banner_Gallery_Info.Where(x => x.Banner_Id == bann.BannerId).Select(x => x.Banner_Image_Path).ToList());
                    bann.Banner_Ralated_To_Text = context.Def_Banner_Related_Info.Where(x => x.Banner_Related_Id == bann.Banner_Related_To).FirstOrDefault().Banner_Related_To;
                    if (bann.Banner_Related_To == 1)
                    {
                        bann.Banner_Related_Value_Text = "Home";
                    }
                    else if (bann.Banner_Related_To == 2)
                    {
                        bann.Banner_Related_Value_Text = context.Def_Super_Categories.Where(x => x.Super_Category_Id == bann.Banner_Related_Value).FirstOrDefault().Super_Category_Name;
                    }
                    else if (bann.Banner_Related_To == 3)
                    {
                        bann.Banner_Related_Value_Text = context.Def_Categories.Where(x => x.Category_Id == bann.Banner_Related_Value).FirstOrDefault().Category_Name;
                    }
                    else if (bann.Banner_Related_To == 4)
                    {
                        bann.Banner_Related_Value_Text = context.Def_Sub_Categories.Where(x => x.Sub_Category_Id == bann.Banner_Related_Value).FirstOrDefault().Sub_Category_Name;
                    }
                    else if (bann.Banner_Related_To == 5)
                    {
                        bann.Banner_Related_Value_Text = context.Def_Brand_Info.Where(x => x.Brand_Id == bann.Banner_Related_Value).FirstOrDefault().Brand_Name;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return banners;
        }

        public int DeleteBanner(long bannerId, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                Tbl_Banner_Gallery_Info gallery = context.Tbl_Banner_Gallery_Info.Where(x => x.Banner_Id == bannerId).FirstOrDefault();
                gallery.Updated_By = userData.User_Id;
                gallery.Updated_Date = DateTime.Now;
                gallery.Is_Active = false;
                gallery.Is_Deleted = true;
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int EditBannerInfo(Tbl_Banner_Gallery_Info bannerGallery, List<string> imagePaths, Tbl_User_Info userData)
        {
            int i = 0;
            try
            {
                foreach (string image in imagePaths)
                {
                    Tbl_Banner_Gallery_Info bannerInfo = context.Tbl_Banner_Gallery_Info.Where(x => x.Banner_Id == bannerGallery.Banner_Id).FirstOrDefault();
                    if (bannerInfo != null)
                    {
                        bannerInfo.Banner_Image_Path = bannerGallery.Banner_Image_Path;
                        bannerInfo.Banner_Related_Id = bannerGallery.Banner_Related_Id;
                        bannerInfo.Banner_Related_Value = bannerGallery.Banner_Related_Value;
                        if (bannerGallery.Is_Enabled != null)
                        {
                            bannerInfo.Is_Enabled = bannerGallery.Is_Enabled;
                        }
                        bannerInfo.Updated_By = userData.User_Id;
                        bannerInfo.Updated_Date = DateTime.Now;
                    }

                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public int RemoveSubProduct(long subProductId)
        {
            int i = 0;
            try
            {
                Tbl_Sub_Product_Info subProductInfo = context.Tbl_Sub_Product_Info.Where(x => x.Sub_Product_Id == subProductId).FirstOrDefault();
                subProductInfo.Is_Active = false;
                subProductInfo.Is_Deleted = true;
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<NotifyProducts> NotifyProducts()
        {
            try
            {
                return context.Database.SqlQuery<NotifyProducts>("Select Notify_Id,N.Product_Id,Product_Name,Name,Email,Mobile,N.Status from [Test_Pharma].[Tbl_Notify_Products] N join [dbo].[Tbl_Product_Info] P on N.Product_Id = P.Product_Id").ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int AddCoupon(Tbl_Coupon_Info couponData)
        {
            int i = 0;
            try
            {
                using (Db_Pharma_MartEntities context = new Db_Pharma_MartEntities())
                {
                    Tbl_Coupon_Info coupon = new Tbl_Coupon_Info()
                    {
                        Coupon_Code = couponData.Coupon_Code,
                        Valid_From = couponData.Valid_From,
                        Valid_To = couponData.Valid_To,
                        Min_Cart_Value = couponData.Min_Cart_Value,
                        Max_Discount_Value = couponData.Max_Discount_Value,
                        Coupon_Discount_Type_Id = couponData.Coupon_Discount_Type_Id,
                        Coupon_Discount_Type_Value = couponData.Coupon_Discount_Type_Value,
                        Only_Particular_Users = couponData.Only_Particular_Users,
                        Total_No_Of_Times_Usage = couponData.Total_No_Of_Times_Usage,
                        No_Of_Usages_Per_User = couponData.No_Of_Usages_Per_User,
                        Created_Date = DateTime.Now,
                        Status = true,
                    };

                    context.Tbl_Coupon_Info.Add(coupon);
                    i = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<Def_Coupon_Discount_Types> getCouponDiscountTypes()
        {
            List<Def_Coupon_Discount_Types> couponDiscountTypes = new List<Def_Coupon_Discount_Types>();
            try
            {
                couponDiscountTypes = context.Def_Coupon_Discount_Types.Where(x => x.Status == true).ToList();
            }
            catch (Exception ex)
            {

            }
            return couponDiscountTypes;
        }

        public List<OrdersList> getPartialOrders(long? OrderId, string TxnStatus, string PaymentMode, int OrderCurrentStatusId, DateTime? FromDate, DateTime? ToDate, string ShipmentId, string CourierName, string PromoCode)
        {
            List<OrdersList> searchedList = getOrders();

            List<OrdersList> result = (from orderList in searchedList
                                       where ((OrderId == 0 || OrderId == null) ? true : (orderList.paymentTransactionId == OrderId))
                                       && ((string.IsNullOrEmpty(TxnStatus) || TxnStatus == "-1") ? true : (orderList.TxnStatus == TxnStatus))
                                       && ((string.IsNullOrEmpty(PaymentMode) || PaymentMode == "0") ? true : (orderList.PaymentMode == PaymentMode))
                                       && ((OrderCurrentStatusId == 0) ? true : (orderList.OrderCurrentStatusId == OrderCurrentStatusId))
                                       && ((FromDate == null || ToDate == null) ? true : (orderList.TxnDate >= FromDate && orderList.TxnDate <= ToDate))
                                       && ((string.IsNullOrEmpty(CourierName) || CourierName == "0") ? true : (orderList.CourierName == CourierName))
                                       && ((string.IsNullOrEmpty(PromoCode)) ? true : (orderList.PromoCode == PromoCode))
                                       select orderList).ToList();

            return (result);
        }

        public List<Def_Order_Current_Status> getOrderCurrentStatusTypes()
        {
            List<Def_Order_Current_Status> OrderCurrentStatusTypes = new List<Def_Order_Current_Status>();
            try
            {
                OrderCurrentStatusTypes = context.Def_Order_Current_Status.Where(x => x.Is_Active == true).ToList();
            }
            catch (Exception ex)
            {

            }
            return OrderCurrentStatusTypes;
        }

        public List<Tbl_Coupon_Info> getAllCoupons()
        {
            List<Tbl_Coupon_Info> couponsList = new List<Tbl_Coupon_Info>();
            try
            {
                couponsList = (from coupon in context.Tbl_Coupon_Info
                               where coupon.Status == true && !coupon.Coupon_Code.StartsWith("CPKG_")
                               select coupon
                              ).ToList();
            }
            catch (Exception ex)
            {

            }
            return couponsList;
        }

        public int DisableCouponCode(long coupon_Id)
        {
            int i = 0;
            try
            {
                Tbl_Coupon_Info coupon = context.Tbl_Coupon_Info.Where(x => x.Coupon_Id == coupon_Id).FirstOrDefault();
                coupon.Status = false;
                i = context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return i;
        }

        public List<TBL_Wallet_Transactions> getUserWallet(long id)
        {
            List<TBL_Wallet_Transactions> walletList = new List<TBL_Wallet_Transactions>();
            try
            {
                walletList = context.TBL_Wallet_Transactions.Where(x => x.Updated_By == id).ToList();
            }
            catch (Exception ex)
            {

            }
            return walletList;
        }

        private decimal GetWalletAmount(long? user_Id)
        {
            try
            {
                decimal walletAmount = (context.TBL_Wallet_Transactions.Where(x => x.Updated_By == user_Id
                                        && x.Is_Deleted == false && x.Credit_Debit == true).Select(x => x.TxnAmount)).DefaultIfEmpty(0).Sum() -
                                        (context.TBL_Wallet_Transactions.Where(x => x.Updated_By == user_Id
                                        && x.Is_Deleted == false && x.Credit_Debit == false).Select(x => x.TxnAmount)).DefaultIfEmpty(0).Sum();

                return Math.Round(walletAmount, 2);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
