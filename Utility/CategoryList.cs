﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
   public class CategoryList
    {
        public long CategoryId { get; set; }
        public int SuperCategoryId { set; get; }
        public string CategoryName { set; get; }
        public string SuperCategoryName { get; set; }        
        public bool? Is_Active { set; get; }
        public bool? Is_Deleted { set; get; }
    }
}
