﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class NotifyProducts
    {
        public long Notify_Id { set; get; }
        public long Product_Id { set; get; }
        public string Product_Name { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public long Mobile { set; get; }
    }
}
