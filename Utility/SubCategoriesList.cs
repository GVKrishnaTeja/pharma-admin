﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class SubCategoriesList
    {
        public int SuperCategoryId { set; get; }
        public int CategoryId { set; get; }
        public long SubCategoryId { set; get; }
        public string SuperCategoryName { set; get; }
        public string CategoryName { set; get; }
        public string SubCategoryName { set; get; }
        public bool? Is_Active { set; get; }
        public bool? Is_Deleted { set; get; }
    }
}
