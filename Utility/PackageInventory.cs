﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Web.Mvc;

namespace Utility
{
   public class PackageInventory
    {
        public long Package_Id { get; set; }
        public string Package_Name { get; set; }
        public int Package_Type_Id { set; get; }
        public DateTime Package_Expiry { get; set; }
        public decimal Actual_Cost { set; get; }
        public decimal Final_Cost { set; get; }
        public decimal Discount_Percentage { set; get; }
        [AllowHtml]
        public string package_Description { set; get; }
        public int package_Quantity { set; get; }
        public List<int> Product_quantity { set; get; }
        public int Max_Count { set; get; }
        public string packageProducts { set; get; }
        public int package_Min_Products_Count { set; get; }
        public decimal package_Min_Amount { set; get; }
        public decimal CashBack_Percentage { set; get; }
    }
}
