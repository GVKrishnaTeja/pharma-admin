//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Payment_Transactions
    {
        public long PaymentTransaction_Id { get; set; }
        public Nullable<long> User_Id { get; set; }
        public Nullable<System.DateTime> Txn_Date { get; set; }
        public Nullable<System.DateTime> Updated_Date { get; set; }
        public string PG_Txn_Id { get; set; }
        public string Txn_Ref_No { get; set; }
        public string Txn_Status { get; set; }
        public string Txn_Message { get; set; }
        public Nullable<decimal> Txn_Amount { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> Other_Charges { get; set; }
        public Nullable<decimal> Shipping_Charges { get; set; }
        public Nullable<int> Order_Current_Status { get; set; }
        public string Payment_Mode { get; set; }
        public string Payment_Gateway { get; set; }
        public Nullable<bool> Payment_Status { get; set; }
        public Nullable<long> Promo_Code_Id { get; set; }
        public Nullable<bool> Authorized { get; set; }
        public string Shipment_Id { get; set; }
        public Nullable<System.DateTime> Shipment_Date { get; set; }
        public Nullable<bool> Pickup { get; set; }
        public string Pickup_Id { get; set; }
        public Nullable<System.DateTime> Pickup_Date { get; set; }
        public Nullable<bool> Dispatched { get; set; }
        public string Dispatched_Id { get; set; }
        public Nullable<System.DateTime> Dispatched_Date { get; set; }
        public Nullable<bool> Delivered { get; set; }
        public Nullable<System.DateTime> Delivered_Date { get; set; }
        public string Delivered_Id { get; set; }
        public string Courier_Name { get; set; }
        public string Courier_Person_Name { get; set; }
        public string Courier_Person_Contact { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<long> Updated_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Promo_Code { get; set; }
        public Nullable<decimal> Promo_Code_Discount { get; set; }
    }
}
