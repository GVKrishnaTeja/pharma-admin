﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Utility
{
    public class ProductInventory
    {
        public long Product_Id { get; set; }
        public string Product_Name { get; set; }
        public Nullable<long> Sub_Category_Id { get; set; }
        public Nullable<bool> Is_Featured { get; set; }
        public Nullable<bool> Have_SubProducts { set; get; }
        public string Product_Code { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> Product_Sub_Type_Id { get; set; }
        [AllowHtml]
        public string Short_Description { get; set; }
        [AllowHtml]
        public string Full_Description { get; set; }
        public Nullable<decimal> Actual_Cost { get; set; }
        public Nullable<decimal> Discount_Percentage { get; set; }
        public Nullable<decimal> Discount_Amount { get; set; }
        public Nullable<decimal> Final_Cost { get; set; }
        [AllowHtml]
        public string Features { get; set; }
        public Nullable<bool> Is_Sold { get; set; }
        public Nullable<long> Brand_Id { get; set; }
        public Nullable<decimal> Shipping_Cost { get; set; }
        [AllowHtml]
        public string Meta_Description { get; set; }
        [AllowHtml]
        public string Meta_Keywords { get; set; }
        public string Page_Title { get; set; }
        public string H1_Tag { get; set; }
        public Nullable<long> Status_Id { get; set; }
        public Nullable<bool> Is_Prescription_Needed { get; set; }
        public string Generic_Name { set; get; }
        public string Product_Sub_Type_Inventory { set; get; }
        public List<SubProductInventory> subProductInfo { set; get; }
    }

    public class SubProductInventory
    {
        public long Sub_Product_Id { set; get; }
        public Nullable<int> Product_Type_Id { set; get; }
        public Nullable<int> Product_Sub_Type_Id { set; get; }
        public string Product_Sub_Type { set; get; }
        public string Product_Sub_Type_Inventory { set; get; }
        public Nullable<decimal> Actual_Cost { set; get; }
        public Nullable<int> Pack_Of_Products_Count { set; get; }
        public Nullable<decimal> Pack_Of_Products_Discount_Percentage { set; get; }
        public Nullable<decimal> Pack_Of_Products_Final_Cost { set; get; }
        public Nullable<int> Sub_Product_Quantity { set; get; }
    }
}
