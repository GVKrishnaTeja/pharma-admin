﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class PackageInfo
    {
        public Tbl_Packages_Info packageDetails { set; get; }
        public List<Tbl_Package_Products> packageProducts { set; get; }
        public List<Tbl_Product_Info> productInfo { set; get; }
        public List<Tbl_Product_Gallery> galleryInfo { set; get; }
    }
}
