﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class OrdersList
    {
        public long paymentTransactionId { get; set; }
        public long userId { set; get; }
        public string userName { set; get; }
        public List<long?> productId { set; get; }
        public List<long?> packageId { set; get; }
        public List<string> productName { set; get; }
        public List<string> packageName { get; set; }
        public DateTime? TxnDate { set; get; }
        public string orderCurrentStatus { set; get; }
        public string TxnStatus { get; set; }
        public decimal? TxnAmount { get; set; }
        public string PaymentMode { set; get; }
        public string ShipmentId { get; set; }
        public string CourierName { get; set; }
        public string PromoCode { get; set; }
        public int OrderCurrentStatusId { get; set; }
    }
}
