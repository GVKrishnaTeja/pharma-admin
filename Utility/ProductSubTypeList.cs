﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class ProductSubTypeList
    {
        public int productTypeId { set; get; }
        public string productType { set; get; }
        public int productSubTypeId { set; get; }
        public string productSubType { set; get; }
        public bool? Is_Active { set; get; }
        public bool? Is_Deleted { set; get; }
    }
}
