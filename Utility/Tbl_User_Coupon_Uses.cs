//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Utility
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_User_Coupon_Uses
    {
        public long User_Redeemed_Id { get; set; }
        public Nullable<long> Coupon_Id { get; set; }
        public Nullable<long> User_Id { get; set; }
        public Nullable<long> PaymentTransaction_Id { get; set; }
        public Nullable<System.DateTime> Coupon_Redemeed_Date { get; set; }
        public Nullable<bool> Status { get; set; }
    }
}
