﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
   public class BannersInfo
    {
        public long BannerId { set; get; }
        public List<string> Banner_Images { set; get; }
        public int? Banner_Related_To { set; get; }
        public string Banner_Ralated_To_Text { set; get; }
        public long? Banner_Related_Value { set; get; }
        public string Banner_Related_Value_Text { set; get; }
        public bool? Is_Enabled { set; get; }
    }
}
