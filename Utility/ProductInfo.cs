﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class ProductInfo
    {
        public Tbl_Product_Info productData { get; set; }
        public Def_Brand_Info brandInfo { get; set; }
        public Def_Sub_Categories subCategoryInfo { get;set; }
        public Def_Categories categoryInfo { get; set; }
        public Def_Super_Categories superCategoryInfo { get; set; }
        public List<Tbl_Product_Gallery> productGallery { get; set; }
        public Def_Product_Type productTypeInfo { get; set; }
        public Tbl_Product_Sub_Types subProductTypeInfo { get; set; }
        public List<SubProductInventory> subProductsInfo { set; get; }
        public string genericName { set; get; }
    }
}
