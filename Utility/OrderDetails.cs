﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class OrderDetails
    {
        public List<Tbl_Product_Info> productsInfo { set; get; }
        public List<Tbl_Packages_Info> packagesInfo { set; get; }
        public Tbl_Payment_Transactions orderInfo { get; set; }
        public List<Tbl_User_Payment_Transactions> paymentInfo { get; set; }
        public Tbl_User_Info userInfo { set; get; }
        public List<Tbl_User_Addresses> userAddresses { set; get; }
        public string orderCurrentStatus { get; set; }
        public List<Tbl_Presecription_Info> prescriptionList { get; set; }
    }
}
